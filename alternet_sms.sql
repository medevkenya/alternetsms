-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 03, 2022 at 05:48 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alternet_sms`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `adminId`, `description`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'Updated group [ Chama Moja ]', 0, 0, '2021-02-06 17:07:22', '2021-02-06 17:07:22'),
(2, 1, 'Added new role [Accounts2]', 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(3, 1, 'Added new role [HR]', 1, 0, '2021-02-12 08:18:47', '2021-02-12 08:18:47'),
(4, 1, 'Added new role [HR2]', 1, 0, '2021-02-12 08:19:32', '2021-02-12 08:19:32'),
(5, 1, 'Added new role [HR23]', 1, 0, '2021-02-12 08:20:00', '2021-02-12 08:20:00'),
(6, 1, 'Added new role [HR233]', 1, 0, '2021-02-12 08:21:07', '2021-02-12 08:21:07'),
(7, 1, 'Added new role [HR2337]', 1, 0, '2021-02-12 08:22:31', '2021-02-12 08:22:31'),
(8, 1, 'Added new role [gigi]', 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(9, 1, 'Added new role [mm]', 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(10, 1, 'Added new role [vvvvvv]', 1, 0, '2021-02-12 08:47:37', '2021-02-12 08:47:37'),
(11, 1, 'Added new role [vvvvvfv]', 1, 0, '2021-02-12 08:47:54', '2021-02-12 08:47:54'),
(12, 1, 'Added new role [vvvvvfvfdfdfd]', 1, 0, '2021-02-12 08:48:12', '2021-02-12 08:48:12'),
(13, 1, 'Added new role [kvd]', 1, 0, '2021-02-12 08:52:00', '2021-02-12 08:52:00'),
(14, 1, 'Added new role [kvd777]', 1, 0, '2021-02-12 08:53:01', '2021-02-12 08:53:01'),
(15, 1, 'Added new role [vbvbvbv]', 1, 0, '2021-02-12 08:55:07', '2021-02-12 08:55:07'),
(16, 1, 'Added new role [vbvbvbv444]', 1, 0, '2021-02-12 08:56:23', '2021-02-12 08:56:23'),
(17, 1, 'Added new role [cvcvcv]', 1, 0, '2021-02-12 09:17:24', '2021-02-12 09:17:24'),
(18, 1, 'Added new role [cvddfdfdf]', 1, 0, '2021-02-12 09:19:04', '2021-02-12 09:19:04'),
(19, 1, 'Added new role [trtrtrt]', 1, 0, '2021-02-12 09:22:48', '2021-02-12 09:22:48'),
(20, 1, 'Edited SMS batch [14]', 1, 0, '2021-02-16 11:30:16', '2021-02-16 11:30:16'),
(21, 1, 'Deleted SMS batch [14]', 1, 0, '2021-02-16 11:30:26', '2021-02-16 11:30:26'),
(22, 1, 'globaltempingservices@gmail.com Logged In', 1, 0, '2021-12-18 15:28:22', '2021-12-18 15:28:22'),
(23, 1, 'globaltempingservices@gmail.com Logged In', 1, 0, '2021-12-19 01:39:11', '2021-12-19 01:39:11'),
(24, 1, 'globaltempingservices@gmail.com Logged In', 1, 0, '2021-12-19 05:56:15', '2021-12-19 05:56:15'),
(25, 1, 'globaltempingservices@gmail.com Logged In', 1, 0, '2021-12-19 10:42:07', '2021-12-19 10:42:07'),
(26, 1, 'globaltempingservices@gmail.com Logged In', 1, 0, '2021-12-20 03:56:58', '2021-12-20 03:56:58'),
(27, 1, 'globaltempingservices@gmail.com Logged In', 1, 0, '2021-12-21 02:03:58', '2021-12-21 02:03:58'),
(28, 1, 'globaltempingservices@gmail.com Logged In', 1, 0, '2021-12-23 05:47:32', '2021-12-23 05:47:32'),
(29, 1, 'Edited contact [708564408]', 1, 0, '2021-12-23 09:13:00', '2021-12-23 09:13:00'),
(30, 1, 'Edited contact [254708564408]', 1, 0, '2021-12-23 09:13:22', '2021-12-23 09:13:22'),
(31, 1, 'globaltempingservices@gmail.com Logged In', 1, 0, '2022-01-03 00:58:24', '2022-01-03 00:58:24'),
(32, 1, 'juhudischools@gmail.com Logged In', 2, 0, '2022-01-03 01:45:37', '2022-01-03 01:45:37');

-- --------------------------------------------------------

--
-- Table structure for table `attendance_list`
--

CREATE TABLE `attendance_list` (
  `id` int(11) NOT NULL,
  `fullName` varchar(50) NOT NULL,
  `mobileNo` varchar(20) DEFAULT NULL,
  `location` varchar(50) NOT NULL,
  `sessionId` int(11) DEFAULT NULL,
  `attendance_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance_list`
--

INSERT INTO `attendance_list` (`id`, `fullName`, `mobileNo`, `location`, `sessionId`, `attendance_date`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'dfsdf', '0702066508', 'Mombasa Rd', NULL, '0000-00-00', 1, 1, '2021-12-20 04:16:35', '2021-12-20 05:36:46'),
(2, 'dfsdf', '0702066508', 'Mombasa Rd', NULL, '0000-00-00', 1, 0, '2021-12-20 04:17:02', '2021-12-20 04:17:02'),
(3, 'dfsdf', '0702066508', 'Mombasa Rd', NULL, '0000-00-00', 1, 0, '2021-12-20 04:17:23', '2021-12-20 04:17:23'),
(4, 'S.O Kenya', '0702066508', 'Mombasa Rd', NULL, '0000-00-00', 1, 0, '2021-12-20 04:21:17', '2021-12-20 04:21:17'),
(5, 'Brian Mulunda', '0711380614', 'Nairobi', 1, '2021-01-01', 1, 0, '2021-12-20 05:35:57', '2021-12-20 05:36:13');

-- --------------------------------------------------------

--
-- Table structure for table `brandnames`
--

CREATE TABLE `brandnames` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `orgName` varchar(200) NOT NULL,
  `brandName` varchar(15) NOT NULL,
  `username` varchar(20) NOT NULL,
  `apikey` varchar(100) NOT NULL,
  `balanceLimit` int(11) NOT NULL DEFAULT '0',
  `accountNo` varchar(100) DEFAULT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brandnames`
--

INSERT INTO `brandnames` (`id`, `adminId`, `created_by`, `orgName`, `brandName`, `username`, `apikey`, `balanceLimit`, `accountNo`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '', 'FLEXYCABSAPP', 'FLEXYCABSAPP', '52dbb92a33d91fc1afcde163d82c2befeab42f227923435ec6952bf4d0ca3dbb', 111, 'jaybee.api', 0, '2021-02-08 07:21:38', '2021-12-19 04:43:59'),
(2, 1, 1, 'The Biggest Organization In The World', 'FLOAT', 'FLOAT', 'fa7e26f86b46b2149d24651f3b101e31c87db6f4ff47550a948d50f3c9db4da2', 10, 'jaybee.api', 0, '2021-02-08 07:21:38', '2021-12-23 12:15:33'),
(3, 3, 1, '', 'COOOL', '12', 'cool', 20, 'jaybee.api', 1, '2021-02-10 09:36:25', '2021-12-19 04:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `groupId` int(11) DEFAULT NULL,
  `contactName` varchar(30) DEFAULT NULL,
  `mobileNo` varchar(15) NOT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL,
  `countyId` int(11) DEFAULT NULL,
  `subCountyId` int(11) DEFAULT NULL,
  `wardId` int(11) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `excelCount` int(11) DEFAULT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `adminId`, `created_by`, `groupId`, `contactName`, `mobileNo`, `dob`, `gender`, `countyId`, `subCountyId`, `wardId`, `location`, `excelCount`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, 'dfdfd', '0114254785', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-08 02:54:57', '2021-02-08 02:54:57'),
(2, 1, 1, 3, 'kevo', '0702066599', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-08 03:07:34', '2021-02-08 03:07:45'),
(3, 1, 1, 3, 'Johny', '0702066599', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-08 10:18:25', '2021-02-08 10:18:25'),
(4, 1, 1, 17, NULL, '702066508', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 03:43:44', '2021-02-09 06:44:44'),
(5, 1, 1, 17, NULL, '702064408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 03:43:44', '2021-02-09 06:44:47'),
(6, 1, 1, 17, NULL, '702774408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 03:43:44', '2021-02-09 06:44:49'),
(7, 1, 1, 17, NULL, '708564408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 03:43:44', '2021-02-09 06:44:52'),
(8, 1, 1, 19, NULL, '702066508', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 03:46:26', '2021-02-09 03:46:26'),
(9, 1, 1, 20, NULL, '702064408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 03:46:26', '2021-02-09 03:46:26'),
(10, 1, 1, 21, NULL, '702774408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 03:46:26', '2021-02-09 03:46:26'),
(11, 1, 1, 22, NULL, '708564408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 03:46:26', '2021-02-09 03:46:26'),
(12, 1, 1, 23, NULL, '702066508', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 03:47:05', '2021-02-09 03:47:05'),
(13, 1, 1, 24, NULL, '702064408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 03:47:05', '2021-02-09 03:47:05'),
(14, 1, 1, 25, NULL, '702774408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 03:47:05', '2021-02-09 03:47:05'),
(15, 1, 1, 26, NULL, '708564408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 03:47:05', '2021-02-09 03:47:05'),
(16, 1, 1, NULL, NULL, '702066508', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-02-09 04:14:22', '2021-02-11 08:15:19'),
(17, 1, 1, NULL, NULL, '702064408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-02-09 04:14:22', '2021-02-11 08:15:14'),
(18, 1, 1, NULL, NULL, '702774408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-02-09 04:14:22', '2021-02-11 08:15:08'),
(19, 1, 1, NULL, NULL, '708564408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-02-09 04:14:22', '2021-02-11 08:15:03'),
(20, 1, 1, 29, NULL, '702066508', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 04:15:58', '2021-02-09 04:15:58'),
(21, 1, 1, 29, NULL, '702064408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 04:15:58', '2021-02-09 04:15:58'),
(22, 1, 1, 29, NULL, '702774408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 04:15:58', '2021-02-09 04:15:58'),
(23, 1, 1, 29, NULL, '708564408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 04:15:58', '2021-02-09 04:15:58'),
(24, 1, 1, 30, NULL, '702066508', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2021-02-09 04:27:52', '2021-02-09 04:27:52'),
(25, 1, 1, 30, NULL, '702064408', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2021-02-09 04:27:52', '2021-02-09 04:27:52'),
(26, 1, 1, 30, NULL, '702774408', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2021-02-09 04:27:52', '2021-02-09 04:27:52'),
(27, 1, 1, 30, NULL, '708564408', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2021-02-09 04:27:52', '2021-02-09 04:27:52'),
(28, 1, 1, 31, NULL, '702066508', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2021-02-09 04:29:09', '2021-02-09 04:29:09'),
(29, 1, 1, 31, NULL, '702064408', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2021-02-09 04:29:09', '2021-02-09 04:29:09'),
(30, 1, 1, 31, NULL, '702774408', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2021-02-09 04:29:09', '2021-02-09 04:29:09'),
(31, 1, 1, 31, NULL, '708564408', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2021-02-09 04:29:09', '2021-02-09 04:29:09'),
(32, 1, 1, 45, 'aaa', '702066508', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 10:51:04', '2021-02-09 10:51:04'),
(33, 1, 1, 45, 'bbbb', '702064408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 10:51:04', '2021-02-09 10:51:04'),
(34, 1, 1, 45, 'vccc', '702774408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 10:51:04', '2021-02-09 10:51:04'),
(35, 1, 1, 45, 'yyyy', '708564408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 10:51:04', '2021-02-09 10:51:04'),
(36, 1, 1, 46, 'aaa', '702066508', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 10:52:03', '2021-02-09 10:52:03'),
(37, 1, 1, 46, 'bbbb', '702064408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 10:52:03', '2021-02-09 10:52:03'),
(38, 1, 1, 46, 'vccc', '702774408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 10:52:03', '2021-02-09 10:52:03'),
(39, 1, 1, 46, 'yyyy', '708564408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 10:52:03', '2021-02-09 10:52:03'),
(40, 1, 1, 47, 'aaa', '702066508', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 10:53:02', '2021-02-09 10:53:02'),
(41, 1, 1, 47, 'bbbb', '702064408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 10:53:02', '2021-02-09 10:53:02'),
(42, 1, 1, 47, 'vccc', '702774408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 10:53:02', '2021-02-09 10:53:02'),
(43, 1, 1, 47, 'yyyy', '708564408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 10:53:02', '2021-02-09 10:53:02'),
(44, 1, 1, 48, 'aaa', '702066508', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 10:59:24', '2021-02-09 10:59:24'),
(45, 1, 1, 48, 'bbbb', '702064408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 10:59:24', '2021-02-09 10:59:24'),
(46, 1, 1, 48, 'vccc', '702774408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 10:59:24', '2021-02-09 10:59:24'),
(47, 1, 1, 48, 'yyyy', '708564408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 10:59:24', '2021-02-09 10:59:24'),
(48, 1, 1, 49, 'aaa', '702066508', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 11:01:26', '2021-02-09 11:01:26'),
(49, 1, 1, 49, 'bbbb', '702064408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 11:01:26', '2021-02-09 11:01:26'),
(50, 1, 1, 49, 'vccc', '702774408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 11:01:26', '2021-02-09 11:01:26'),
(51, 1, 1, 49, 'yyyy', '708564408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 11:01:26', '2021-02-09 11:01:26'),
(52, 1, 1, 50, 'aaa', '702066508', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 11:01:46', '2021-02-09 11:01:46'),
(53, 1, 1, 50, 'bbbb', '702064408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 11:01:46', '2021-02-09 11:01:46'),
(54, 1, 1, 50, 'vccc', '702774408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 11:01:46', '2021-02-09 11:01:46'),
(55, 1, 1, 50, 'yyyy', '708564408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 11:01:46', '2021-02-09 11:01:46'),
(56, 1, 1, 51, 'aaa', '702066508', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 11:02:54', '2021-02-09 11:02:54'),
(57, 1, 1, 51, 'bbbb', '702064408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 11:02:54', '2021-02-09 11:02:54'),
(58, 1, 1, 51, 'vccc', '702774408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 11:02:54', '2021-02-09 11:02:54'),
(59, 1, 1, 51, 'yyyy', '708564408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 11:02:54', '2021-02-09 11:02:54'),
(60, 1, 1, 52, 'aaa', '702066508', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 11:05:07', '2021-02-09 11:05:07'),
(61, 1, 1, 52, 'bbbb', '702064408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 11:05:07', '2021-02-09 11:05:07'),
(62, 1, 1, 52, 'vccc', '702774408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-09 11:05:07', '2021-02-09 11:05:07'),
(63, 1, 1, 52, 'Emmanuel Kibicho Murungi', '254708564408', '2021-01-01', 'Male', NULL, NULL, NULL, 'Nairobi Kenya', NULL, 0, '2021-02-09 11:05:07', '2021-12-23 09:13:22');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `groupName` text NOT NULL,
  `description` text,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `adminId`, `created_by`, `groupName`, `description`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Test Group', '', 1, '2021-02-06 17:34:11', '2021-02-08 05:13:54'),
(2, 1, 1, 'Wazito Group', 'Sema', 0, '2021-02-06 17:34:11', '2021-02-08 05:13:00'),
(3, 1, 1, 'Faulu', 'Its acool', 0, '2021-02-08 05:11:23', '2021-02-08 05:11:23'),
(17, 1, 1, 'Excel Group 14V2KA - 2021-02-09', NULL, 0, '2021-02-09 03:43:44', '2021-02-09 03:43:44'),
(18, 1, 1, 'Excel Group 51D3G7 - 2021-02-09', NULL, 0, '2021-02-09 03:45:07', '2021-02-09 03:45:07'),
(19, 1, 1, 'Excel Group YF6AP8 - 2021-02-09', NULL, 0, '2021-02-09 03:46:26', '2021-02-09 03:46:26'),
(20, 1, 1, 'Excel Group 2S11PU - 2021-02-09', NULL, 0, '2021-02-09 03:46:26', '2021-02-09 03:46:26'),
(21, 1, 1, 'Excel Group NQDRFU - 2021-02-09', NULL, 0, '2021-02-09 03:46:26', '2021-02-09 03:46:26'),
(22, 1, 1, 'Excel Group 3DF1TE - 2021-02-09', NULL, 0, '2021-02-09 03:46:26', '2021-02-09 03:46:26'),
(23, 1, 1, 'Excel Group 2ZH8BN - 2021-02-09', NULL, 0, '2021-02-09 03:47:05', '2021-02-09 03:47:05'),
(24, 1, 1, 'Excel Group 31HP12 - 2021-02-09', NULL, 0, '2021-02-09 03:47:05', '2021-02-09 03:47:05'),
(25, 1, 1, 'Excel Group HR8221 - 2021-02-09', NULL, 0, '2021-02-09 03:47:05', '2021-02-09 03:47:05'),
(26, 1, 1, 'Excel Group BP8VHF - 2021-02-09', NULL, 0, '2021-02-09 03:47:05', '2021-02-09 03:47:05'),
(27, 1, 1, 'Excel Group HGWV56 - 2021-02-09', NULL, 0, '2021-02-09 03:53:41', '2021-02-09 03:53:41'),
(28, 1, 1, 'Excel Group 1UY8ED - 2021-02-09', NULL, 0, '2021-02-09 04:14:22', '2021-02-09 04:14:22'),
(29, 1, 1, 'Excel Group TPSNG8 - 2021-02-09', NULL, 0, '2021-02-09 04:15:58', '2021-02-09 04:15:58'),
(30, 1, 1, 'Excel Group 1D5U2A - 2021-02-09', NULL, 0, '2021-02-09 04:27:52', '2021-02-09 04:27:52'),
(31, 1, 1, 'Excel Group B1NFC4 - 2021-02-09', NULL, 0, '2021-02-09 04:29:09', '2021-02-09 04:29:09'),
(32, 1, 1, 'Excel Group VPFBN2 - 2021-02-09', NULL, 0, '2021-02-09 04:30:28', '2021-02-09 04:30:28'),
(33, 1, 1, 'Excel Group 6YVS20 - 2021-02-09', NULL, 0, '2021-02-09 04:33:46', '2021-02-09 04:33:46'),
(34, 1, 1, 'Excel Group ZI6DBY - 2021-02-09', NULL, 0, '2021-02-09 04:37:44', '2021-02-09 04:37:44'),
(35, 1, 1, 'Excel Group 6B1RH7 - 2021-02-09', NULL, 0, '2021-02-09 04:39:36', '2021-02-09 04:39:36'),
(36, 1, 1, 'Excel Group VBQCE4 - 2021-02-09', NULL, 0, '2021-02-09 04:40:05', '2021-02-09 04:40:05'),
(37, 1, 1, 'Excel Group KQ12RB - 2021-02-09', NULL, 0, '2021-02-09 04:45:03', '2021-02-09 04:45:03'),
(38, 1, 1, 'Excel Group V7TYZX - 2021-02-09', NULL, 0, '2021-02-09 04:45:47', '2021-02-09 04:45:47'),
(39, 1, 1, 'Excel Group 28VD1H - 2021-02-09', NULL, 0, '2021-02-09 04:46:25', '2021-02-09 04:46:25'),
(40, 1, 1, 'Excel Group S8LF5Y - 2021-02-09', NULL, 0, '2021-02-09 04:47:10', '2021-02-09 04:47:10'),
(41, 1, 1, 'Excel Group K1JXAS - 2021-02-09', NULL, 0, '2021-02-09 04:47:56', '2021-02-09 04:47:56'),
(42, 1, 1, 'Excel Group NHE2OT - 2021-02-09', NULL, 0, '2021-02-09 07:49:03', '2021-02-09 07:49:03'),
(43, 1, 1, 'Excel Group 17ZQ1F - 2021-02-09', NULL, 0, '2021-02-09 07:50:27', '2021-02-09 07:50:27'),
(44, 1, 1, 'Excel Group 6E4HLB - 2021-02-09', NULL, 0, '2021-02-09 07:50:41', '2021-02-09 07:50:41'),
(45, 1, 1, 'Excel Group 8LTIQJ - 2021-02-09', NULL, 0, '2021-02-09 07:51:03', '2021-02-09 07:51:03'),
(46, 1, 1, 'Excel Group 7TEFIN - 2021-02-09', NULL, 0, '2021-02-09 07:52:03', '2021-02-09 07:52:03'),
(47, 1, 1, 'Excel Group JQUG2F - 2021-02-09', NULL, 0, '2021-02-09 07:53:01', '2021-02-09 07:53:01'),
(48, 1, 1, 'Excel Group SXAYC6 - 2021-02-09', NULL, 0, '2021-02-09 07:59:23', '2021-02-09 07:59:23'),
(49, 1, 1, 'Excel Group 1AQTRP - 2021-02-09', NULL, 0, '2021-02-09 08:01:26', '2021-02-09 08:01:26'),
(50, 1, 1, 'Excel Group 5SW0CG - 2021-02-09', NULL, 0, '2021-02-09 08:01:46', '2021-02-09 08:01:46'),
(51, 1, 1, 'Excel Group MBOAXS - 2021-02-09', NULL, 0, '2021-02-09 08:02:54', '2021-02-09 08:02:54'),
(52, 1, 1, 'Excel Group 1G6J7F - 2021-02-09', NULL, 0, '2021-02-09 08:05:07', '2021-02-09 08:05:07'),
(53, 1, 1, 'vvvvvv', 'yyyyy', 1, '2021-12-12 09:21:45', '2021-12-12 09:22:00');

-- --------------------------------------------------------

--
-- Table structure for table `membership`
--

CREATE TABLE `membership` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `location` varchar(50) NOT NULL,
  `mobileNo` text,
  `idNo` varchar(20) DEFAULT NULL,
  `gender` varchar(6) NOT NULL,
  `position` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `moduleName` varchar(30) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `moduleName`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Schools', 0, '2021-02-06 16:01:32', '2021-02-06 16:01:32'),
(2, 'Colleges', 0, '2021-02-06 16:01:32', '2021-02-06 16:01:32'),
(3, 'Churches', 0, '2021-02-06 16:01:44', '2021-02-06 16:01:44'),
(4, 'Hospitals', 0, '2021-02-06 16:01:44', '2021-02-06 16:01:44'),
(5, 'Businesses', 0, '2021-02-06 16:02:01', '2021-02-06 16:02:01'),
(6, 'Hotels', 0, '2021-02-06 16:02:01', '2021-02-06 16:02:01'),
(7, 'Chamas', 0, '2021-02-06 16:05:06', '2021-02-06 16:05:06'),
(8, 'Marketing', 0, '2021-02-06 16:05:06', '2021-02-06 16:05:06');

-- --------------------------------------------------------

--
-- Table structure for table `outbox`
--

CREATE TABLE `outbox` (
  `id` int(11) NOT NULL,
  `ref_queue_id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `senderId` int(11) NOT NULL,
  `contactId` int(11) DEFAULT NULL,
  `groupId` int(11) DEFAULT NULL,
  `mobileNo` varchar(15) NOT NULL,
  `message` text NOT NULL,
  `cost` double NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outbox`
--

INSERT INTO `outbox` (`id`, `ref_queue_id`, `adminId`, `senderId`, `contactId`, `groupId`, `mobileNo`, `message`, `cost`, `status`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 1, 1, 1, '254702066508', 'We have a group meeting tomorrow at 10:00 am', 0.8, 0, 0, '2021-02-08 06:58:33', '2021-02-08 06:58:33'),
(2, 13, 1, 2, 60, 52, '702066508', 'Same old shit 11111', 0.8, 0, 0, '2021-02-14 10:02:21', '2021-02-14 10:02:21'),
(3, 13, 1, 2, 61, 52, '702064408', 'Same old shit 11111', 0.8, 0, 0, '2021-02-14 10:02:21', '2021-02-14 10:02:21'),
(4, 13, 1, 2, 62, 52, '702774408', 'Same old shit 11111', 0.8, 0, 0, '2021-02-14 10:02:21', '2021-02-14 10:02:21'),
(5, 13, 1, 2, 63, 52, '708564408', 'Same old shit 11111', 0.8, 0, 0, '2021-02-14 10:02:21', '2021-02-14 10:02:21'),
(6, 14, 1, 1, 60, 52, '702066508', 'bbb bbb  bbbb', 0.8, 0, 0, '2021-02-14 13:32:30', '2021-02-14 13:32:30'),
(7, 14, 1, 1, 61, 52, '702064408', 'bbb bbb  bbbb', 0.8, 0, 0, '2021-02-14 13:32:30', '2021-02-14 13:32:30'),
(8, 14, 1, 1, 62, 52, '702774408', 'bbb bbb  bbbb', 0.8, 0, 0, '2021-02-14 13:32:30', '2021-02-14 13:32:30'),
(9, 14, 1, 1, 63, 52, '708564408', 'bbb bbb  bbbb', 0.8, 0, 0, '2021-02-14 13:32:30', '2021-02-14 13:32:30');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `description` text NOT NULL,
  `days` int(11) NOT NULL,
  `months` int(3) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `amount`, `description`, `days`, `months`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Trial', '0.00', 'Good to start with', 1, 0, 0, '2021-02-09 13:06:51', '2021-02-10 11:53:46'),
(2, 'Starter', '1000.00', 'Good to start with', 30, 1, 0, '2021-02-09 13:06:51', '2021-02-12 13:50:34'),
(3, 'Basic', '2500.00', 'Good to start with', 90, 3, 0, '2021-02-09 13:06:51', '2021-02-12 13:50:37'),
(4, 'Professional', '5000.00', 'Good to start with', 180, 6, 0, '2021-02-09 13:06:51', '2021-02-12 13:50:40'),
(5, 'Ultra', '10000.00', 'Good to start with', 365, 12, 0, '2021-02-09 13:06:51', '2021-02-12 13:50:43');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `packageId` int(11) NOT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `reference` varchar(50) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `fee` decimal(10,2) DEFAULT NULL,
  `method` varchar(20) DEFAULT NULL,
  `currency` varchar(5) NOT NULL,
  `details` text,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `userId`, `packageId`, `transaction_id`, `reference`, `amount`, `fee`, `method`, `currency`, `details`, `status`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 22, 1, NULL, 'NM70AG', '100.00', NULL, NULL, 'KES', NULL, 0, 0, '2020-11-21 07:21:14', '2020-11-21 07:21:14'),
(2, 22, 1, NULL, '265QOX', '100.00', NULL, NULL, 'KES', NULL, 0, 0, '2020-11-21 07:22:30', '2020-11-21 07:22:30'),
(3, 22, 2, NULL, '20924P', '200.00', NULL, NULL, 'KES', NULL, 0, 0, '2020-11-21 07:26:41', '2020-11-21 07:26:41'),
(4, 22, 3, NULL, '229UK5', '500.00', NULL, NULL, 'KES', NULL, 0, 0, '2020-11-21 07:32:45', '2020-11-21 07:32:45'),
(5, 22, 2, NULL, 'RO99Q1', '200.00', NULL, NULL, 'KES', NULL, 0, 0, '2020-11-21 07:36:23', '2020-11-21 07:36:23'),
(6, 22, 1, NULL, 'VUFG9B', '100.00', NULL, NULL, 'KES', NULL, 0, 0, '2020-11-21 07:38:01', '2020-11-21 07:38:01'),
(7, 22, 1, NULL, 'EZ2I02', '100.00', NULL, NULL, 'KES', NULL, 0, 0, '2020-11-21 07:39:02', '2020-11-21 07:39:02'),
(8, 22, 1, NULL, 'XM5D51', '100.00', NULL, NULL, 'KES', NULL, 0, 0, '2020-11-21 07:41:18', '2020-11-21 07:41:18'),
(9, 22, 1, NULL, 'I121CJ', '100.00', NULL, NULL, 'KES', NULL, 0, 0, '2020-11-21 08:45:53', '2020-11-21 08:45:53'),
(10, 22, 2, NULL, 'GXSVOE', '200.00', NULL, NULL, 'KES', NULL, 0, 0, '2020-11-21 08:49:36', '2020-11-21 08:49:36'),
(11, 22, 2, '1714726', 'M2VE9J', '200.00', '5.80', NULL, 'KES', '{\"status\":\"success\",\"message\":\"Tx Fetched\",\"data\":{\"txid\":1714726,\"txref\":\"M2VE9J\",\"flwref\":\"8502220835\",\"devicefingerprint\":\"e287a57eb0b652f6570943ddc1b14f03\",\"cycle\":\"one-time\",\"amount\":200,\"currency\":\"KES\",\"chargedamount\":205.8,\"appfee\":5.8,\"merchantfee\":0,\"merchantbearsfee\":0,\"chargecode\":\"00\",\"chargemessage\":\"Successful\",\"authmodel\":\"LIPA_MPESA\",\"ip\":\"105.163.87.93\",\"narration\":\"FLW-PBF MPESA Transaction \",\"status\":\"successful\",\"vbvcode\":\"N/A\",\"vbvmessage\":\"N/A\",\"authurl\":\"N/A\",\"acctcode\":\"00\",\"acctmessage\":\"Successful\",\"paymenttype\":\"mpesa\",\"paymentid\":\"N/A\",\"fraudstatus\":\"ok\",\"chargetype\":\"normal\",\"createdday\":6,\"createddayname\":\"SATURDAY\",\"createdweek\":47,\"createdmonth\":10,\"createdmonthname\":\"NOVEMBER\",\"createdquarter\":4,\"createdyear\":2020,\"createdyearisleap\":true,\"createddayispublicholiday\":0,\"createdhour\":11,\"createdminute\":56,\"createdpmam\":\"am\",\"created\":\"2020-11-21T11:56:29.000Z\",\"customerid\":531404,\"custphone\":\"254702066508\",\"custnetworkprovider\":\"UNKNOWN PROVIDER\",\"custname\":\"Brian Medev\",\"custemail\":\"medevkenya@gmail.com\",\"custemailprovider\":\"GMAIL\",\"custcreated\":\"2020-11-21T10:28:54.000Z\",\"accountid\":220897,\"acctbusinessname\":\"Brian Mulunda\",\"acctcontactperson\":\"Brian Mulunda\",\"acctcountry\":\"KE\",\"acctbearsfeeattransactiontime\":0,\"acctparent\":2410,\"acctvpcmerchant\":\"N/A\",\"acctalias\":null,\"acctisliveapproved\":0,\"orderref\":\"8502220835\",\"paymentplan\":null,\"paymentpage\":null,\"raveref\":null,\"amountsettledforthistransaction\":200,\"meta\":[]}}', 1, 0, '2020-11-21 08:54:18', '2020-11-21 08:54:48'),
(12, 22, 5, '1714755', 'MTXYLR', '1000.00', '29.00', NULL, 'KES', '{\"status\":\"success\",\"message\":\"Tx Fetched\",\"data\":{\"txid\":1714755,\"txref\":\"MTXYLR\",\"flwref\":\"2597613808\",\"devicefingerprint\":\"e287a57eb0b652f6570943ddc1b14f03\",\"cycle\":\"one-time\",\"amount\":1000,\"currency\":\"KES\",\"chargedamount\":1029,\"appfee\":29,\"merchantfee\":0,\"merchantbearsfee\":0,\"chargecode\":\"00\",\"chargemessage\":\"Successful\",\"authmodel\":\"LIPA_MPESA\",\"ip\":\"105.163.87.93\",\"narration\":\"FLW-PBF MPESA Transaction \",\"status\":\"successful\",\"vbvcode\":\"N/A\",\"vbvmessage\":\"N/A\",\"authurl\":\"N/A\",\"acctcode\":\"00\",\"acctmessage\":\"Successful\",\"paymenttype\":\"mpesa\",\"paymentid\":\"N/A\",\"fraudstatus\":\"ok\",\"chargetype\":\"normal\",\"createdday\":6,\"createddayname\":\"SATURDAY\",\"createdweek\":47,\"createdmonth\":10,\"createdmonthname\":\"NOVEMBER\",\"createdquarter\":4,\"createdyear\":2020,\"createdyearisleap\":true,\"createddayispublicholiday\":0,\"createdhour\":12,\"createdminute\":17,\"createdpmam\":\"pm\",\"created\":\"2020-11-21T12:17:24.000Z\",\"customerid\":531404,\"custphone\":\"254702066508\",\"custnetworkprovider\":\"UNKNOWN PROVIDER\",\"custname\":\"Brian Medev\",\"custemail\":\"medevkenya@gmail.com\",\"custemailprovider\":\"GMAIL\",\"custcreated\":\"2020-11-21T10:28:54.000Z\",\"accountid\":220897,\"acctbusinessname\":\"Brian Mulunda\",\"acctcontactperson\":\"Brian Mulunda\",\"acctcountry\":\"KE\",\"acctbearsfeeattransactiontime\":0,\"acctparent\":2410,\"acctvpcmerchant\":\"N/A\",\"acctalias\":null,\"acctisliveapproved\":0,\"orderref\":\"2597613808\",\"paymentplan\":null,\"paymentpage\":null,\"raveref\":null,\"amountsettledforthistransaction\":1000,\"meta\":[]}}', 1, 0, '2020-11-21 09:15:16', '2020-11-21 09:15:45'),
(13, 22, 6, '1714762', '0EMG0F', '2500.00', '72.50', NULL, 'KES', '{\"status\":\"success\",\"message\":\"Tx Fetched\",\"data\":{\"txid\":1714762,\"txref\":\"0EMG0F\",\"flwref\":\"4329379027\",\"devicefingerprint\":\"e287a57eb0b652f6570943ddc1b14f03\",\"cycle\":\"one-time\",\"amount\":2500,\"currency\":\"KES\",\"chargedamount\":2572.5,\"appfee\":72.5,\"merchantfee\":0,\"merchantbearsfee\":0,\"chargecode\":\"00\",\"chargemessage\":\"Successful\",\"authmodel\":\"LIPA_MPESA\",\"ip\":\"105.163.87.93\",\"narration\":\"FLW-PBF MPESA Transaction \",\"status\":\"successful\",\"vbvcode\":\"N/A\",\"vbvmessage\":\"N/A\",\"authurl\":\"N/A\",\"acctcode\":\"00\",\"acctmessage\":\"Successful\",\"paymenttype\":\"mpesa\",\"paymentid\":\"N/A\",\"fraudstatus\":\"ok\",\"chargetype\":\"normal\",\"createdday\":6,\"createddayname\":\"SATURDAY\",\"createdweek\":47,\"createdmonth\":10,\"createdmonthname\":\"NOVEMBER\",\"createdquarter\":4,\"createdyear\":2020,\"createdyearisleap\":true,\"createddayispublicholiday\":0,\"createdhour\":12,\"createdminute\":20,\"createdpmam\":\"pm\",\"created\":\"2020-11-21T12:20:20.000Z\",\"customerid\":531404,\"custphone\":\"254702066508\",\"custnetworkprovider\":\"UNKNOWN PROVIDER\",\"custname\":\"Brian Medev\",\"custemail\":\"medevkenya@gmail.com\",\"custemailprovider\":\"GMAIL\",\"custcreated\":\"2020-11-21T10:28:54.000Z\",\"accountid\":220897,\"acctbusinessname\":\"Brian Mulunda\",\"acctcontactperson\":\"Brian Mulunda\",\"acctcountry\":\"KE\",\"acctbearsfeeattransactiontime\":0,\"acctparent\":2410,\"acctvpcmerchant\":\"N/A\",\"acctalias\":null,\"acctisliveapproved\":0,\"orderref\":\"4329379027\",\"paymentplan\":null,\"paymentpage\":null,\"raveref\":null,\"amountsettledforthistransaction\":2500,\"meta\":[]}}', 1, 0, '2020-11-21 09:18:14', '2020-11-21 09:18:39'),
(14, 22, 4, '1714767', '6M25X6', '500.00', '14.50', NULL, 'KES', '{\"status\":\"success\",\"message\":\"Tx Fetched\",\"data\":{\"txid\":1714767,\"txref\":\"6M25X6\",\"flwref\":\"2386281338\",\"devicefingerprint\":\"e287a57eb0b652f6570943ddc1b14f03\",\"cycle\":\"one-time\",\"amount\":500,\"currency\":\"KES\",\"chargedamount\":514.5,\"appfee\":14.5,\"merchantfee\":0,\"merchantbearsfee\":0,\"chargecode\":\"00\",\"chargemessage\":\"Successful\",\"authmodel\":\"LIPA_MPESA\",\"ip\":\"105.163.87.93\",\"narration\":\"FLW-PBF MPESA Transaction \",\"status\":\"successful\",\"vbvcode\":\"N/A\",\"vbvmessage\":\"N/A\",\"authurl\":\"N/A\",\"acctcode\":\"00\",\"acctmessage\":\"Successful\",\"paymenttype\":\"mpesa\",\"paymentid\":\"N/A\",\"fraudstatus\":\"ok\",\"chargetype\":\"normal\",\"createdday\":6,\"createddayname\":\"SATURDAY\",\"createdweek\":47,\"createdmonth\":10,\"createdmonthname\":\"NOVEMBER\",\"createdquarter\":4,\"createdyear\":2020,\"createdyearisleap\":true,\"createddayispublicholiday\":0,\"createdhour\":12,\"createdminute\":23,\"createdpmam\":\"pm\",\"created\":\"2020-11-21T12:23:13.000Z\",\"customerid\":531404,\"custphone\":\"254702066508\",\"custnetworkprovider\":\"UNKNOWN PROVIDER\",\"custname\":\"Brian Medev\",\"custemail\":\"medevkenya@gmail.com\",\"custemailprovider\":\"GMAIL\",\"custcreated\":\"2020-11-21T10:28:54.000Z\",\"accountid\":220897,\"acctbusinessname\":\"Brian Mulunda\",\"acctcontactperson\":\"Brian Mulunda\",\"acctcountry\":\"KE\",\"acctbearsfeeattransactiontime\":0,\"acctparent\":2410,\"acctvpcmerchant\":\"N/A\",\"acctalias\":null,\"acctisliveapproved\":0,\"orderref\":\"2386281338\",\"paymentplan\":null,\"paymentpage\":null,\"raveref\":null,\"amountsettledforthistransaction\":500,\"meta\":[]}}', 1, 0, '2020-11-21 09:21:07', '2020-11-21 09:21:34'),
(15, 1, 3, NULL, 'HGQOMA', '2500.00', NULL, NULL, 'KES', NULL, 0, 0, '2021-02-12 10:43:32', '2021-02-12 10:43:32'),
(16, 1, 2, NULL, '7J26F6', '1000.00', NULL, NULL, 'KES', NULL, 0, 0, '2021-02-12 10:47:17', '2021-02-12 10:47:17'),
(17, 1, 5, NULL, 'VEKLRD', '10000.00', NULL, NULL, 'KES', NULL, 0, 0, '2021-02-12 10:47:22', '2021-02-12 10:47:22'),
(18, 1, 4, NULL, 'DK3R4G', '6000.00', NULL, NULL, 'KES', NULL, 0, 0, '2021-02-12 10:47:23', '2021-02-12 10:47:23'),
(19, 1, 3, NULL, 'OEGA11', '2500.00', NULL, NULL, 'KES', NULL, 0, 0, '2021-02-12 10:47:24', '2021-02-12 10:47:24'),
(20, 1, 5, '1904540', '756HGT', '10000.00', '290.00', NULL, 'KES', '{\"status\":\"success\",\"message\":\"Tx Fetched\",\"data\":{\"txid\":1904540,\"txref\":\"756HGT\",\"flwref\":\"9632118379\",\"devicefingerprint\":\"8c7fab8cd27b8d566ea73de128aef9f0\",\"cycle\":\"one-time\",\"amount\":10000,\"currency\":\"KES\",\"chargedamount\":10290,\"appfee\":290,\"merchantfee\":0,\"merchantbearsfee\":0,\"chargecode\":\"00\",\"chargemessage\":\"Successful\",\"authmodel\":\"LIPA_MPESA\",\"ip\":\"197.232.84.223\",\"narration\":\"FLW-PBF MPESA Transaction \",\"status\":\"successful\",\"vbvcode\":\"N/A\",\"vbvmessage\":\"N/A\",\"authurl\":\"N/A\",\"acctcode\":\"00\",\"acctmessage\":\"Successful\",\"paymenttype\":\"mpesa\",\"paymentid\":\"N/A\",\"fraudstatus\":\"ok\",\"chargetype\":\"normal\",\"createdday\":5,\"createddayname\":\"FRIDAY\",\"createdweek\":6,\"createdmonth\":1,\"createdmonthname\":\"FEBRUARY\",\"createdquarter\":1,\"createdyear\":2021,\"createdyearisleap\":false,\"createddayispublicholiday\":0,\"createdhour\":13,\"createdminute\":48,\"createdpmam\":\"pm\",\"created\":\"2021-02-12T13:48:44.000Z\",\"customerid\":590581,\"custphone\":\"254702066508\",\"custnetworkprovider\":\"UNKNOWN PROVIDER\",\"custname\":\"Global12 Services\",\"custemail\":\"globaltempingservices@gmail.com\",\"custemailprovider\":\"GMAIL\",\"custcreated\":\"2021-02-12T13:48:43.000Z\",\"accountid\":220897,\"acctbusinessname\":\"Brian Mulunda\",\"acctcontactperson\":\"Brian Mulunda\",\"acctcountry\":\"KE\",\"acctbearsfeeattransactiontime\":0,\"acctparent\":2410,\"acctvpcmerchant\":\"N/A\",\"acctalias\":null,\"acctisliveapproved\":0,\"orderref\":\"9632118379\",\"paymentplan\":null,\"paymentpage\":null,\"raveref\":null,\"amountsettledforthistransaction\":10000,\"meta\":[]}}', 1, 0, '2021-02-12 10:47:54', '2021-02-12 10:48:32'),
(21, 1, 2, NULL, 'QRTW8J', '1000.00', NULL, NULL, 'KES', NULL, 0, 0, '2021-02-12 11:02:37', '2021-02-12 11:02:37'),
(22, 1, 2, NULL, 'KRD3J7', '1000.00', NULL, NULL, 'KES', NULL, 0, 0, '2021-10-20 10:01:10', '2021-10-20 10:01:10'),
(23, 1, 2, NULL, '637PJ8', '1000.00', NULL, NULL, 'KES', NULL, 0, 0, '2021-10-20 10:01:11', '2021-10-20 10:01:11'),
(24, 1, 2, NULL, 'TGXZWL', '1000.00', NULL, NULL, 'KES', NULL, 0, 0, '2021-10-20 10:01:12', '2021-10-20 10:01:12'),
(25, 1, 3, NULL, 'JZQ4RS', '2500.00', NULL, NULL, 'KES', NULL, 0, 0, '2021-10-20 10:01:13', '2021-10-20 10:01:13'),
(26, 1, 4, NULL, 'WKYBMH', '6000.00', NULL, NULL, 'KES', NULL, 0, 0, '2021-10-20 10:01:14', '2021-10-20 10:01:14');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `permissionName` varchar(30) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `permissionName`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'SMS Single Recipient', 0, '2021-02-10 13:32:56', '2021-02-10 13:32:56'),
(2, 'SMS Group', 0, '2021-02-10 13:32:56', '2021-02-10 13:32:56'),
(3, 'SMS Excel Contacts', 0, '2021-02-10 13:33:40', '2021-02-12 12:21:02'),
(4, 'Create Group', 0, '2021-02-10 13:33:40', '2021-02-12 12:21:05'),
(5, 'Edit Group', 0, '2021-02-10 13:33:54', '2021-02-10 13:33:54'),
(6, 'Delete Group', 0, '2021-02-10 13:33:54', '2021-02-10 13:33:54'),
(7, 'Create Contact', 0, '2021-02-10 13:34:11', '2021-02-10 13:34:11'),
(8, 'Edit Contact', 0, '2021-02-10 13:34:11', '2021-02-10 13:34:11'),
(9, 'Delete Contact', 0, '2021-02-10 13:35:31', '2021-02-10 13:35:31'),
(10, 'Outbox', 0, '2021-02-10 13:35:31', '2021-02-10 13:35:31'),
(11, 'Subscriptions', 0, '2021-02-10 13:36:01', '2021-02-10 13:36:01'),
(12, 'Settings', 0, '2021-02-10 13:36:01', '2021-02-10 13:36:01'),
(13, 'Users', 0, '2021-02-10 13:36:17', '2021-02-10 13:36:17'),
(14, 'Roles', 0, '2021-02-10 13:36:17', '2021-02-10 13:36:17'),
(15, 'Permissions', 0, '2021-02-10 13:36:24', '2021-02-10 13:36:24'),
(16, 'View Group', 0, '2021-02-10 13:36:24', '2021-02-10 13:36:24'),
(17, 'View Contact', 0, '2021-02-10 13:36:24', '2021-02-10 13:36:24');

-- --------------------------------------------------------

--
-- Table structure for table `ref_queue`
--

CREATE TABLE `ref_queue` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `message` text NOT NULL,
  `senderId` int(11) NOT NULL,
  `groupId` int(11) DEFAULT NULL,
  `overall_status` int(11) DEFAULT '0',
  `batch_size` int(11) DEFAULT '0',
  `batch_sent` int(11) DEFAULT '0',
  `batch_processed` int(11) DEFAULT '0',
  `fullTrial` int(11) NOT NULL DEFAULT '0',
  `totalCost` decimal(10,2) NOT NULL,
  `send_date` date DEFAULT NULL,
  `send_time` time DEFAULT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_queue`
--

INSERT INTO `ref_queue` (`id`, `adminId`, `created_by`, `message`, `senderId`, `groupId`, `overall_status`, `batch_size`, `batch_sent`, `batch_processed`, `fullTrial`, `totalCost`, `send_date`, `send_time`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 0, 0, 'Test sms to me', 0, 0, 1, 1, 1, 1, 0, '0.00', '2020-05-08', '21:22:00', 0, '2021-02-08 12:27:03', '2021-02-08 12:27:03'),
(2, 0, 0, 'Test sms to me number 2', 0, 0, 1, 1, 1, 1, 0, '0.00', '2020-05-08', '21:22:00', 0, '2021-02-08 12:27:03', '2021-02-08 12:27:03'),
(3, 1, 1, 'dsfsd svsfsdf', 2, 0, 1, 1, 0, 0, 0, '0.00', '2021-02-08', '12:27:00', 0, '2021-02-08 09:27:51', '2021-02-08 09:27:51'),
(4, 1, 1, 'its all good', 1, 3, 1, 1, 0, 0, 0, '9.60', '2021-02-08', '13:24:00', 0, '2021-02-08 10:24:00', '2021-02-08 10:24:00'),
(5, 1, 1, 'vvvvvv bbbbbb', 1, 48, 1, 1, 0, 0, 0, '10.40', '2021-02-09', '10:59:00', 0, '2021-02-09 07:59:24', '2021-02-09 07:59:24'),
(6, 1, 1, 'vvvv ggggg ttttt', 1, 50, 1, 4, 0, 0, 0, '51.20', '2021-02-09', '11:01:00', 0, '2021-02-09 08:01:46', '2021-02-09 08:01:46'),
(7, 1, 1, 'mmmmmmmmmmmmmmmmmmmmmmmmm', 1, 51, 1, 4, 0, 0, 0, '0.50', '2021-02-09', '11:02:00', 0, '2021-02-09 08:02:54', '2021-02-09 08:02:54'),
(8, 1, 1, 'wwwwww wwwww', 2, 52, 1, 4, 0, 0, 0, '3.20', '2021-02-09', '11:05:00', 0, '2021-02-09 08:05:07', '2021-02-09 08:05:07'),
(9, 1, 1, 'Inform everyone to come 2022', 1, NULL, 0, 0, 0, 0, 0, '0.00', '2021-02-14', '09:37:00', 0, '2021-02-14 06:37:16', '2021-02-14 06:37:16'),
(10, 1, 1, 'cool tena', 1, 52, 0, 4, 0, 0, 0, '3.20', '2021-02-14', '09:39:00', 0, '2021-02-14 06:39:21', '2021-02-14 06:39:21'),
(11, 1, 1, 'Same old shit 11111', 2, 52, 1, 4, 0, 0, 0, '3.20', '2021-02-14', '10:00:00', 0, '2021-02-14 07:00:14', '2021-02-14 07:00:14'),
(12, 1, 1, 'Same old shit 11111', 2, 52, 1, 4, 0, 0, 0, '3.20', '2021-02-14', '10:00:00', 0, '2021-02-14 07:00:33', '2021-02-14 07:00:33'),
(13, 1, 1, 'Same old shit 11111', 2, 52, 1, 4, 0, 0, 0, '3.20', '2021-02-14', '10:02:00', 0, '2021-02-14 07:02:21', '2021-02-14 07:02:21'),
(14, 1, 1, 'bbb bbb  bbbb 888888', 1, 52, 0, 4, 0, 0, 0, '3.20', NULL, NULL, 1, '2021-02-14 10:32:30', '2021-02-16 11:30:26');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `roleName` varchar(30) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `roleName`, `description`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Admin', NULL, 1, 1, 0, '2021-02-10 10:03:25', '2021-02-10 10:03:25'),
(2, 'Operations 2', NULL, 1, 1, 0, '2021-02-10 10:03:36', '2021-02-11 05:31:29'),
(3, 'Secretary', NULL, 1, 1, 1, '2021-02-11 03:27:02', '2021-02-11 03:33:25'),
(4, 'OPS', NULL, 1, 1, 1, '2021-02-11 03:29:35', '2021-02-11 03:32:30'),
(5, 'Manager', NULL, 1, 1, 0, '2021-02-12 04:23:38', '2021-02-12 04:23:38'),
(6, 'Accounts', NULL, 1, 1, 0, '2021-02-12 04:32:42', '2021-02-12 04:32:42'),
(7, 'Accounts2', NULL, 1, 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(8, 'HR', NULL, 1, 1, 0, '2021-02-12 08:18:47', '2021-02-12 08:18:47'),
(9, 'HR2', NULL, 1, 1, 0, '2021-02-12 08:19:32', '2021-02-12 08:19:32'),
(10, 'HR23', NULL, 1, 1, 0, '2021-02-12 08:20:00', '2021-02-12 08:20:00'),
(11, 'HR233', NULL, 1, 1, 0, '2021-02-12 08:21:07', '2021-02-12 08:21:07'),
(12, 'HR2337', NULL, 1, 1, 0, '2021-02-12 08:22:31', '2021-02-12 08:22:31'),
(13, 'gigi', NULL, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(14, 'mm', NULL, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(15, 'vvvvvv', NULL, 1, 1, 0, '2021-02-12 08:47:37', '2021-02-12 08:47:37'),
(16, 'vvvvvfv', NULL, 1, 1, 0, '2021-02-12 08:47:54', '2021-02-12 08:47:54'),
(17, 'vvvvvfvfdfdfd', NULL, 1, 1, 0, '2021-02-12 08:48:12', '2021-02-12 08:48:12'),
(18, 'kvd', NULL, 1, 1, 0, '2021-02-12 08:52:00', '2021-02-12 08:52:00'),
(19, 'kvd777', NULL, 1, 1, 0, '2021-02-12 08:53:01', '2021-02-12 08:53:01'),
(20, 'vbvbvbv', NULL, 1, 1, 0, '2021-02-12 08:55:07', '2021-02-12 08:55:07'),
(21, 'vbvbvbv444', NULL, 1, 1, 0, '2021-02-12 08:56:23', '2021-02-12 08:56:23'),
(22, 'cvcvcv', NULL, 1, 1, 0, '2021-02-12 09:17:24', '2021-02-12 09:17:24'),
(23, 'cvddfdfdf', NULL, 1, 1, 0, '2021-02-12 09:19:04', '2021-02-12 09:19:04'),
(24, 'trtrtrt', NULL, 1, 1, 0, '2021-02-12 09:22:48', '2021-02-12 09:22:48');

-- --------------------------------------------------------

--
-- Table structure for table `sessions_list`
--

CREATE TABLE `sessions_list` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions_list`
--

INSERT INTO `sessions_list` (`id`, `name`, `description`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'cool name', 'cool des', 1, 0, '2021-12-20 04:34:51', '2021-12-20 04:34:51'),
(2, 'mass ya 7', '7 am', 1, 1, '2021-12-20 04:35:06', '2021-12-20 04:35:43');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `balanceLimit` int(11) NOT NULL DEFAULT '10',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `adminId`, `balanceLimit`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 10, 0, '2021-02-08 12:23:05', '2021-02-08 12:23:05');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `packageId` int(11) NOT NULL,
  `subscriptionDate` date NOT NULL,
  `expiryDate` date NOT NULL,
  `status` enum('pending','active','expired','') NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `adminId`, `packageId`, `subscriptionDate`, `expiryDate`, `status`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2021-02-09', '2021-02-28', 'active', 0, '2021-02-09 13:07:15', '2021-02-09 13:07:15');

-- --------------------------------------------------------

--
-- Table structure for table `userpermissions`
--

CREATE TABLE `userpermissions` (
  `id` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userpermissions`
--

INSERT INTO `userpermissions` (`id`, `roleId`, `permissionId`, `adminId`, `created_by`, `status`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(2, 1, 2, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(3, 1, 3, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(4, 1, 4, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-14 09:27:17'),
(5, 1, 5, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(6, 1, 6, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-14 09:27:20'),
(7, 1, 7, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(8, 1, 8, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(9, 1, 9, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(10, 1, 10, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(11, 1, 11, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(12, 1, 12, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(13, 1, 13, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(14, 1, 14, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(15, 1, 15, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(16, 2, 1, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:19:24'),
(17, 2, 2, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(18, 2, 3, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:18:28'),
(19, 2, 4, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:20:52'),
(20, 2, 5, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(21, 2, 6, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:21:01'),
(22, 2, 7, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(23, 2, 8, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:20:54'),
(24, 2, 9, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(25, 2, 10, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(26, 2, 11, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(27, 2, 12, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(28, 2, 13, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:20:25'),
(29, 2, 14, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(30, 2, 15, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(31, 3, 1, 1, 1, 0, 1, '2021-02-11 03:27:02', '2021-02-11 03:33:25'),
(32, 3, 2, 1, 1, 0, 1, '2021-02-11 03:27:02', '2021-02-11 03:33:25'),
(33, 3, 3, 1, 1, 0, 1, '2021-02-11 03:27:02', '2021-02-11 03:33:25'),
(34, 7, 1, 1, 1, 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(35, 7, 2, 1, 1, 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(36, 7, 5, 1, 1, 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(37, 7, 6, 1, 1, 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(38, 7, 7, 1, 1, 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(39, 12, 0, 1, 1, 1, 0, '2021-02-12 08:22:31', '2021-02-12 08:22:31'),
(40, 13, 1, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(41, 13, 2, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(42, 13, 5, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(43, 13, 6, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(44, 13, 7, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(45, 13, 8, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(46, 13, 9, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(47, 13, 10, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(48, 13, 11, 1, 1, 1, 0, '2021-02-12 08:33:56', '2021-02-12 08:33:56'),
(49, 13, 12, 1, 1, 1, 0, '2021-02-12 08:33:56', '2021-02-12 08:33:56'),
(50, 13, 13, 1, 1, 1, 0, '2021-02-12 08:33:56', '2021-02-12 08:33:56'),
(51, 14, 1, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(52, 14, 2, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(53, 14, 5, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(54, 14, 6, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(55, 14, 7, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(56, 14, 8, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(57, 14, 9, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(58, 14, 10, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(59, 14, 11, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(60, 14, 12, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(61, 14, 13, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(62, 14, 14, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(63, 17, 1, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(64, 17, 2, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(65, 17, 5, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(66, 17, 6, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(67, 17, 7, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(68, 17, 8, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(69, 17, 9, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(70, 17, 10, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(71, 17, 11, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(72, 17, 12, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(73, 17, 13, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(74, 17, 14, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(75, 17, 15, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(76, 17, 16, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(77, 17, 17, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(78, 19, 1, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(79, 19, 2, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(80, 19, 5, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(81, 19, 6, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(82, 19, 7, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(83, 19, 8, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(84, 19, 9, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(85, 19, 10, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(86, 19, 11, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(87, 19, 12, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(88, 19, 13, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(89, 19, 14, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(90, 19, 15, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(91, 19, 16, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(92, 19, 17, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(93, 19, 18, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(94, 19, 19, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(95, 21, 1, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(96, 21, 2, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(97, 21, 5, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(98, 21, 6, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(99, 21, 7, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(100, 21, 8, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(101, 21, 9, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(102, 21, 10, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(103, 21, 11, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(104, 21, 12, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(105, 21, 13, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(106, 21, 14, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(107, 21, 15, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(108, 21, 16, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(109, 21, 17, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(110, 21, 18, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(111, 21, 19, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(112, 21, 20, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(113, 21, 21, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(114, 22, 1, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(115, 22, 2, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(116, 22, 5, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(117, 22, 6, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(118, 22, 7, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(119, 22, 8, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(120, 22, 9, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(121, 22, 10, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(122, 22, 11, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(123, 22, 12, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(124, 22, 13, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(125, 22, 14, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(126, 22, 15, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(127, 22, 16, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(128, 22, 17, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(129, 22, 18, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(130, 22, 19, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(131, 22, 20, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(132, 22, 21, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(133, 22, 22, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(134, 23, 1, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(135, 23, 2, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(136, 23, 5, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(137, 23, 6, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(138, 23, 7, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(139, 23, 8, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(140, 23, 9, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(141, 23, 10, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(142, 23, 11, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(143, 23, 12, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(144, 23, 13, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(145, 23, 14, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(146, 23, 15, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(147, 23, 16, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(148, 23, 17, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(149, 23, 18, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(150, 23, 19, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(151, 23, 20, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(152, 23, 21, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(153, 23, 22, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(154, 23, 23, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(155, 1, 16, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(156, 1, 17, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(157, 1, 18, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `adminId` int(11) DEFAULT NULL,
  `firstName` varchar(20) NOT NULL,
  `lastName` varchar(20) NOT NULL,
  `mobileNo` varchar(15) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `resetCode` varchar(100) DEFAULT NULL,
  `isVerified` tinyint(3) NOT NULL DEFAULT '0',
  `isVetted` tinyint(3) NOT NULL DEFAULT '0',
  `isDisabled` tinyint(3) NOT NULL DEFAULT '0',
  `isActive` tinyint(3) NOT NULL DEFAULT '0',
  `wardId` int(11) DEFAULT NULL,
  `userTypeId` enum('Admin','School','Church','Business') NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `roleId` tinyint(3) NOT NULL DEFAULT '1',
  `location` varchar(200) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `profilePic` varchar(30) NOT NULL DEFAULT 'profile.jpg',
  `countryId` int(11) NOT NULL DEFAULT '1',
  `townId` int(11) NOT NULL DEFAULT '1',
  `about` varchar(255) DEFAULT NULL,
  `isOnline` tinyint(3) NOT NULL DEFAULT '0',
  `companyId` int(11) DEFAULT NULL,
  `confirmpassword` varchar(100) DEFAULT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `adminId`, `firstName`, `lastName`, `mobileNo`, `email`, `password`, `resetCode`, `isVerified`, `isVetted`, `isDisabled`, `isActive`, `wardId`, `userTypeId`, `created_by`, `roleId`, `location`, `latitude`, `longitude`, `profilePic`, `countryId`, `townId`, `about`, `isOnline`, `companyId`, `confirmpassword`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'Alternet', 'Limited', '254724619842', 'globaltempingservices@gmail.com', '$2y$10$r07R1c6vQ4HDJnTecCm82.FPR/7zEnGQE87wDU9.bPMn5jamHv0SC', '5a0917b6e81dc7dd29be2cf266c860f1', 1, 0, 0, 1, 0, 'Church', NULL, 1, 'Nairobi, Kenya', -1.2920659, 36.8219462, 'profile.jpg', 1, 1, 'Advertisement of all job categories\r\nA jobs advertisement company', 1, NULL, NULL, 0, '2020-08-09 05:47:37', '2022-01-03 04:44:17'),
(2, 1, 'Juhudi', 'Schools', '254724619842', 'juhudischools@gmail.com', '$2y$10$r07R1c6vQ4HDJnTecCm82.FPR/7zEnGQE87wDU9.bPMn5jamHv0SC', '5a0917b6e81dc7dd29be2cf266c860f1', 1, 0, 0, 1, 0, 'School', NULL, 1, 'Nairobi, Kenya', -1.2920659, 36.8219462, 'profile.jpg', 1, 1, 'Advertisement of all job categories\r\nA jobs advertisement company', 1, NULL, NULL, 0, '2020-08-09 05:47:37', '2022-01-03 01:45:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendance_list`
--
ALTER TABLE `attendance_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brandnames`
--
ALTER TABLE `brandnames`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `membership`
--
ALTER TABLE `membership`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outbox`
--
ALTER TABLE `outbox`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ref_queue`
--
ALTER TABLE `ref_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions_list`
--
ALTER TABLE `sessions_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userpermissions`
--
ALTER TABLE `userpermissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `attendance_list`
--
ALTER TABLE `attendance_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `brandnames`
--
ALTER TABLE `brandnames`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `membership`
--
ALTER TABLE `membership`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `outbox`
--
ALTER TABLE `outbox`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `ref_queue`
--
ALTER TABLE `ref_queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `sessions_list`
--
ALTER TABLE `sessions_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `userpermissions`
--
ALTER TABLE `userpermissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
