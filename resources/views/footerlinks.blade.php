<!-- base:js -->
<script src="{{ URL::asset('assets/vendors/js/vendor.bundle.base.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{ URL::asset('assets/js/off-canvas.js')}}"></script>
<script src="{{ URL::asset('assets/js/hoverable-collapse.js')}}"></script>
<script src="{{ URL::asset('assets/js/template.js')}}"></script>
<script src="{{ URL::asset('assets/js/settings.js')}}"></script>
<script src="{{ URL::asset('assets/js/todolist.js')}}"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<!-- <script src="{{ URL::asset('assets/vendors/progressbar.js/progressbar.min.js')}}"></script>
<script src="{{ URL::asset('assets/vendors/chart.js/Chart.min.js')}}"></script> -->
<!-- End plugin js for this page -->
<!-- Custom js for this page-->
<!-- <script src="{{ URL::asset('assets/js/dashboard.js')}}"></script> -->
<!-- End custom js for this page-->
