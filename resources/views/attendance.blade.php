<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo env("APP_NAME"); ?> | Attendance</title>
    @include('headerlinks')
  </head>
  <body>
    @include('promo')
    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      @include('topnav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_settings-panel.html -->
        @include('themesettings')
        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
        @include('sidenav')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold">Attendance</h3>
                <p>List of attendee statistics and sessions</p>
              </div>
              <div class="col-sm-6">
                <div class="d-flex align-items-center justify-content-md-end">
                  <!-- <div class="mb-3 mb-xl-0 pr-1">
                      <div class="dropdown">
                        <button class="btn bg-white btn-sm dropdown-toggle btn-icon-text border mr-2" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="typcn typcn-calendar-outline mr-2"></i>Last 7 days
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton3" data-x-placement="top-start">
                          <h6 class="dropdown-header">Last 14 days</h6>
                          <a class="dropdown-item" href="#">Last 21 days</a>
                          <a class="dropdown-item" href="#">Last 28 days</a>
                        </div>
                      </div>
                  </div> -->
                  <div class="pr-1 mb-3 mr-2 mb-xl-0">
                    <a href="{{URL::to('/topup')}}" class="btn btn-sm bg-white btn-icon-text border"><i class="typcn typcn-arrow-forward-outline mr-2"></i>Topup SMS Units</a>
                  </div>
                  <div class="pr-1 mb-3 mb-xl-0">
                    <a href="{{URL::to('/attendance')}}" class="btn btn-sm bg-white btn-icon-text border"><i class="typcn typcn-info-large-outline mr-2"></i>Attendance</a>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">

              @if (session('status0'))
              <div class="col-md-12">
              <div class="alert alert-danger alert-dismissible alertbox" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {{ session('status0') }}
              </div>
              </div>
              @endif

              @if (session('status1'))
              <div class="col-md-12">
              <div class="alert alert-success alert-dismissible alertbox" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {{ session('status1') }}
              </div>
              </div>
              @endif

              @if ($message = Session::get('error'))
              <div class="col-md-12">
              <div class="alert alert-warning alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {{ $message }}
              </div>
              </div>
              @endif

              @if ($message = Session::get('success'))
              <div class="col-md-12">
              <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {{ $message }}
              </div>
              </div>
              @endif

              <div class="col-xl-3 d-flex grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex flex-wrap justify-content-between">
                      <h4 class="card-title mb-3">Last 12 Months</h4>
                    </div>
                    <div class="row">
                      <div class="col-12">
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="d-flex justify-content-between mb-4">
                              <div class="font-weight-medium">Month</div>
                              <div class="font-weight-medium">Total</div>
                            </div>
                           <?php foreach ($monthsdata as $key_month) { ?>
                            <div class="d-flex justify-content-between mb-4">
                              <div class="text-secondary font-weight-medium"><?php echo $key_month['month_name']; ?></div>
                              <div class="small"><?php echo $key_month['count']; ?></div>
                            </div>
                            <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-5 d-flex grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex flex-wrap justify-content-between">
                      <h4 class="card-title mb-3">Masses / Sessions</h4>
                      <!-- <a href="{{URL::to('/smshistory')}}">
                        <button type="button" class="btn btn-sm btn-light">View History</button>
                      </a> -->
                    </div>
                    <div class="row">
                      <div class="col-12">

                        <div class="row">
                          <div class="col-sm-12">
                            <div class="d-flex align-items-center justify-content-md-end">
                              <!-- <div class="pr-1 mb-3 mr-2 mb-xl-0">
                                <a href="{{URL::to('/topup')}}" class="btn btn-sm bg-white btn-icon-text border"><i class="typcn typcn-arrow-forward-outline mr-2"></i>Topup SMS Units</a>
                              </div> -->
                              <div class="pr-1 mb-3 mb-xl-0">
                                <a data-toggle="modal" data-target="#defaultadd" class="btn btn-sm bg-primary btn-icon-text border white-text"><i class="typcn typcn-info-large-outline mr-2"></i>Add New</a>
                              </div>
                            </div>
                          </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade text-left" id="defaultadd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            {!! Form::open(['url' => 'addsession']) !!}
                          <div class="modal-content">
                            <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel1">Add</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                            <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12">
                            <fieldset class="form-group position-relative has-icon-left">
                              <h5>Name</h5>
                                <input type="text" name="name" class="form-control" required>
                            </fieldset>
                          </div>
                          <!-- <div class="col-xl-12 col-lg-12 col-md-12">
                          <fieldset class="form-group position-relative has-icon-left">
                            <h5>Description</h5>
                              <textarea rows="6" name="description" class="form-control" required></textarea>
                          </fieldset>
                        </div> -->
                        </div>
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                          </div>
                          {!! Form::close() !!}
                          </div>
                        </div>

                        <div class="table-responsive pt-3">



                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>Name</th>
                                <!-- <th>Description</th> -->
                                <th>Actions</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php $list = \App\Sessionslist::getAll(); foreach ($list as $item) { ?>
                                  <tr>
                                    <td><?php echo $item->name; ?></td>
                                    <!-- <td><?php //echo $item->description; ?></td> -->
                                    <td width="31.6%">
                                      <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-openedit<?php echo $item->id; ?>">Edit</button>

                                      <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-opendelete<?php echo $item->id; ?>">Delete</button>

                                    </td>
                                  </tr>

                                  <!-- Modal -->
                                  <div class="modal fade text-left" id="modal-openedit<?php echo $item->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                    {!! Form::open(['url' => 'editsession']) !!}
                                    <div class="modal-content">
                                      <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel1">Edit</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                      </div>
                                      <div class="modal-body">
                                      <div class="row">
                                      <div class="col-xl-12 col-lg-12 col-md-12">
                                        <input type="hidden" name="id" value="<?php echo $item->id; ?>" class="form-control" required>
                                      <fieldset class="form-group position-relative has-icon-left">
                                        <h5>Name</h5>
                                          <input type="text" name="name" value="<?php echo $item->name; ?>" class="form-control" required>
                                      </fieldset>
                                    </div>
                                    <!-- <div class="col-xl-12 col-lg-12 col-md-12">
                                    <fieldset class="form-group position-relative has-icon-left">
                                      <h5>Description</h5>
                                        <textarea rows="6" name="description" class="form-control" required><?php echo $item->description; ?></textarea>
                                    </fieldset>
                                  </div> -->
                                    </div>
                                      </div>
                                      <div class="modal-footer">
                                      <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Save Changes</button>
                                      </div>
                                    </div>
                                    {!! Form::close() !!}
                                    </div>
                                  </div>

                                  <!-- Modal -->
                                  <div class="modal fade text-left" id="modal-opendelete<?php echo $item->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                      {!! Form::open(['url' => 'deletesession']) !!}
                                    <div class="modal-content">
                                      <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel1">Delete</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                      </div>
                                      <div class="modal-body">
                                      <div class="row">
                                      <div class="col-xl-6 col-lg-6 col-md-6">
                                        <input type="hidden" name="id" value="<?php echo $item->id; ?>" class="form-control" required>
                                    </div>
                                    <h5>Confirm that you want to delete this record</h5>
                                  </div>
                                      </div>
                                      <div class="modal-footer">
                                      <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Delete</button>
                                      </div>
                                    </div>
                                    {!! Form::close() !!}
                                    </div>
                                  </div>

                                  <?php } ?>
                            </tbody>
                          </table>
                          <div class="marginTop-2">
                          {{ $list->links() }}
                        </div>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-4 d-flex grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex flex-wrap justify-content-between">
                      <h4 class="card-title mb-3">Record New Attendance</h4>
                    </div>
                    <div class="row">
                      <div class="col-12">
                        <div class="mb-5">

                          {!! Form::open(['url' => 'addattendance']) !!}

                           <div class="col-xl-12 col-lg-12 col-md-12">
                            <fieldset class="form-group">
                              <select class="custom-select" name="sessionId">
                                <option value="">--Session (Optional)--</option>
                                   <?php foreach ($list as $keyf) { ?>
                                     <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->name; ?></option>
                                   <?php } ?>
                                 </select>
                            </fieldset>
                          </div>

                          <div class="col-xl-12 col-lg-12 col-md-12">
                          <fieldset class="form-group">
                            <!-- <h5>Full Name</h5> -->
                              <input type="text" placeholder="Full Name" name="fullName" class="form-control" value="{{old('fullName')}}" required>
                          </fieldset>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12">
                          <fieldset class="form-group">
                            <!-- <h5>Phone No.</h5> -->
                              <input type="number" placeholder="Phone No." name="mobileNo"  value="{{old('mobileNo')}}" class="form-control">
                          </fieldset>
                      </div>
                      <div class="col-xl-12 col-lg-12 col-md-12">
                      <fieldset class="form-group">
                        <!-- <h5>Location</h5> -->
                          <input type="text" placeholder="Location" name="location"  value="{{old('location')}}" class="form-control" required>
                      </fieldset>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12">
                    <fieldset class="form-group">
                      <!-- <h5>Location</h5> -->
                        <input type="text" placeholder="Date" name="date"  value="{{old('date')}}" class="form-control maindate" required>
                    </fieldset>
                  </div>

                    <div class="col-xl-12 col-lg-12 col-md-12">
                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                  </div>

                </form>

                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
          @include('footer')
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    @include('footerlinks')

  </body>
</html>
