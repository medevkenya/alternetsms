<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo env("APP_NAME"); ?> | SMS Batch Centre</title>
    @include('headerlinks')
  </head>
  <body>
    @include('promo')
    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      @include('topnav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_settings-panel.html -->
        @include('themesettings')
        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
        @include('sidenav')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold">SMS Batch Centre</h3>
                <p>List of sms batches</p>
              </div>



              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <!-- <h4 class="card-title">Bordered table</h4> -->
                    <div class="table-responsive pt-3">

                        @if (session('status0'))
                          <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          {{ session('status0') }}
                          </div>
                        @endif

                        @if (session('status1'))
                          <div class="alert alert-success alert-dismissible alertbox" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          {{ session('status1') }}
                          </div>
                        @endif


                        <div class="table-responsive pt-3">

                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>Group</th>
                                <!-- <th>Brand Name</th> -->
                                <th>Message</th>
                                <th>Total</th>
                                <th>Sent</th>
                                <th>Failed</th>
                                <th>Cost</th>
                                <th>Date/Time</th>
                                <th>Status</th>
                                <th>Actions</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php $batchs = \App\SMS::getAll(); foreach ($batchs as $batch) { ?>
                              <tr>
                                <td><?php echo $batch->groupName; ?></td>
                                <!-- <td><?php //echo $batch->brandName; ?></td> -->
                                <td><?php echo $batch->message; ?></td>
                                <td><?php echo $batch->batch_size; ?></td>
                                <td><?php echo $batch->batch_sent; ?></td>
                                <td><?php echo $batch->fullTrial; ?></td>
                                <td><?php echo $batch->totalCost; ?></td>
                                <td><?php echo $batch->send_date; ?> <?php echo $batch->send_time; ?></td>
                                <td>
                                <?php if($batch->overall_status==0) { ?>
                                  <badge class="btn btn-primary btn-sm" style="padding: 0.1rem 0.1rem">active</badge>
                                <?php } else { ?>
                                  <badge class="btn btn-warning btn-sm" style="padding: 0.1rem 0.1rem;color:#ffffff;">completed</badge>
                                <?php }  ?>
                                </td>
                                <td width="11.6%">
                                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-openedit<?php echo $batch->id; ?>">Edit</button>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-opendelete<?php echo $batch->id; ?>">Delete</button>
                                </td>
                              </tr>

                              <!-- Modal -->
                                                  <div class="modal fade text-left" id="modal-openedit<?php echo $batch->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                      {!! Form::open(['url' => 'editbatch']) !!}
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                      <h4 class="modal-title" id="myModalLabel1">Edit batch</h4>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                      </button>
                                                      </div>
                                                      <div class="modal-body">
                                                      <div class="row">
                                                        <input type="hidden" name="id" value="<?php echo $batch->id; ?>" class="form-control" required>

                                                    <div class="col-xl-12 col-lg-12 col-md-12">
                                                    <fieldset class="form-group">
                                                      <h5>Group</h5>
                                                      <select class="form-control" name="groupId" required>
                                                           <option value="<?php echo $batch->groupId; ?>"><?php echo $batch->groupName; ?></option>
                                                           <?php $groups = \App\Groups::getAll(0); foreach ($groups as $keyf) { ?>
                                                             <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->groupName; ?></option>
                                                           <?php } ?>
                                                         </select>
                                                    </fieldset>
                                                  </div>
                                                  <div class="col-xl-12 col-lg-12 col-md-12">
                                                  <fieldset class="form-group">
                                                    <h5>Message</h5>
                                                      <textarea rows="6" name="message" class="form-control" required><?php echo $batch->message; ?></textarea>
                                                  </fieldset>
                                                </div>
                                                <?php date_default_timezone_set("Africa/Nairobi"); if($batch->send_date >= date('Y-m-d') && $batch->send_time < date('H:i')) { ?>
                                                <div class="col-xl-12 col-lg-12 col-md-12">
                                                <fieldset class="form-group">
                                                  <h5>Scheduled Date</h5>
                                                    <input type="date" id="date" class="form-control"  placeholder="mm-dd-yyyy" value="<?php echo $batch->send_date;?>" name="send_date" required>
                                                </fieldset>
                                              </div>
                                              <div class="col-xl-12 col-lg-12 col-md-12">
                                              <fieldset class="form-group">
                                                <h5>Scheduled Time</h5>
                                                  <input type="text" id="time" class="form-control"  placeholder="eg 22:50" value="<?php echo $batch->send_time;?>" name="send_time" required>
                                              </fieldset>
                                                </div>
                                              <?php } ?>
                                              <div class="col-xl-12 col-lg-12 col-md-12">
                                              <fieldset class="form-group">
                                                <h5>Status (status 0 means active, 1 means completed)</h5>
                                                <select class="form-control" name="overall_status" required>
                                                     <option value="<?php echo $batch->overall_status; ?>"><?php echo $batch->overall_status; ?></option>
                                                       <option value="1">1</option>
                                                       <option value="0">0</option>
                                                   </select>
                                              </fieldset>
                                            </div>
                                                    </div>
                                                      </div>
                                                      <div class="modal-footer">
                                                      <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                      <button type="submit" class="btn btn-primary">Save Changes</button>
                                                      </div>
                                                    </div>
                                                    {!! Form::close() !!}
                                                    </div>
                                                  </div>

                                                  <!-- Modal -->
                                                  <div class="modal fade text-left" id="modal-opendelete<?php echo $batch->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                      {!! Form::open(['url' => 'deletebatch']) !!}
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                      <h4 class="modal-title" id="myModalLabel1">Delete batch</h4>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                      </button>
                                                      </div>
                                                      <div class="modal-body">
                                                      <div class="row">
                                                      <div class="col-xl-6 col-lg-6 col-md-6">
                                                        <input type="hidden" name="id" value="<?php echo $batch->id; ?>" class="form-control" required>
                                                    </div>
                                                    <h5>Confirm that you want to delete this batch</h5>
                                                  </div>
                                                      </div>
                                                      <div class="modal-footer">
                                                      <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                      <button type="submit" class="btn btn-primary">Delete</button>
                                                      </div>
                                                    </div>
                                                    {!! Form::close() !!}
                                                    </div>
                                                  </div>

                              <?php } ?>
                            </tbody>
                          </table>
                          <div class="marginTop-2">
                          {{ $batchs->links() }}
                        </div>

                        </div>

                    </div>
                  </div>
                </div>
              </div>

            </div>



          </div>
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
          @include('footer')
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    @include('footerlinks')
  </body>
</html>
