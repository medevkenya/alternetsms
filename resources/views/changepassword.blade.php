<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo env("APP_NAME"); ?> | Change Password</title>
    @include('headerlinks')
  </head>
  <body>
    @include('promo')
    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      @include('topnav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_settings-panel.html -->
        @include('themesettings')
        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
        @include('sidenav')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold">Change Password</h3>
              </div>
              <div class="col-sm-6">
                <div class="d-flex align-items-center justify-content-md-end">
                  <!-- <div class="mb-3 mb-xl-0 pr-1">
                      <div class="dropdown">
                        <button class="btn bg-white btn-sm dropdown-toggle btn-icon-text border mr-2" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="typcn typcn-calendar-outline mr-2"></i>Last 7 days
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton3" data-x-placement="top-start">
                          <h6 class="dropdown-header">Last 14 days</h6>
                          <a class="dropdown-item" href="#">Last 21 days</a>
                          <a class="dropdown-item" href="#">Last 28 days</a>
                        </div>
                      </div>
                  </div> -->
                  <!-- <div class="pr-1 mb-3 mr-2 mb-xl-0">
                    <a href="{{URL::to('/topup')}}" class="btn btn-sm bg-white btn-icon-text border"><i class="typcn typcn-arrow-forward-outline mr-2"></i>Topup SMS Units</a>
                  </div> -->
                  <!-- <div class="pr-1 mb-3 mb-xl-0">
                    <a href="{{URL::to('/attendance')}}" class="btn btn-sm bg-white btn-icon-text border"><i class="typcn typcn-info-large-outline mr-2"></i>Attendance</a>
                  </div> -->
                </div>
              </div>
            </div>

            <div class="row" style="margin-top:2%;">

            <div class="col-xl-9 d-flex grid-margin stretch-card">
              <div class="card">
                <div class="card-body">

                  <div class="row">
                    <div class="col-12">

                      @if ($message = Session::get('error'))
                      <div class="col-md-12">
                      <div class="alert alert-warning alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      {{ $message }}
                      </div>
                      </div>
                      @endif

                      @if ($message = Session::get('success'))
                      <div class="col-md-12">
                      <div class="alert alert-success alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      {{ $message }}
                      </div>
                      </div>
                      @endif

                      <!-- <p>Fill the following fields</p> -->

                      {!! Form::open(['url' => 'doChangePass']) !!}

                      <div class="row">

                      <div class="col-xl-4 col-lg-4 col-md-4">
                      <fieldset class="form-group position-relative has-icon-left">
                        <h5>Current Password</h5>
                        <input type="password" name="oldpassword" class="form-control" required>
                      </fieldset>
                      </div>

                      <div class="col-xl-4 col-lg-4 col-md-4">
                      <fieldset class="form-group position-relative has-icon-left">
                        <h5>New Password</h5>
                        <input type="password" name="password" class="form-control" required>
                      </fieldset>
                      </div>

                      <div class="col-xl-4 col-lg-4 col-md-4">
                      <fieldset class="form-group position-relative has-icon-left">
                        <h5>Repeat New Password</h5>
                        <input type="password" name="cpassword" class="form-control" required>
                      </fieldset>
                      </div>

                      <div class="col-xl-12 col-lg-12 col-md-12">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>

                      </div>

                      {!! Form::close() !!}

                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

          </div>
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
          @include('footer')
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    @include('footerlinks')

  </body>
</html>
