<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo env("APP_NAME"); ?></title>
    @include('headerlinks')
  </head>
  <body>
    @include('promo')
    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      @include('topnav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_settings-panel.html -->
        @include('themesettings')
        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
        @include('sidenav')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold">Manage Contacts</h3>
                <p>List of all your contacts</p>
              </div>
              <div class="col-sm-6">
                <div class="d-flex align-items-center justify-content-md-end">
                  <!-- <div class="mb-3 mb-xl-0 pr-1">
                      <div class="dropdown">
                        <button class="btn bg-white btn-sm dropdown-toggle btn-icon-text border mr-2" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="typcn typcn-calendar-outline mr-2"></i>Last 7 days
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton3" data-x-placement="top-start">
                          <h6 class="dropdown-header">Last 14 days</h6>
                          <a class="dropdown-item" href="#">Last 21 days</a>
                          <a class="dropdown-item" href="#">Last 28 days</a>
                        </div>
                      </div>
                  </div> -->
                  <div class="pr-1 mb-3 mr-2 mb-xl-0">
                    <button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#exportdata"><i class="typcn typcn-arrow-forward-outline mr-2"></i> Export Contacts</button>
                  </div>
                  <div class="pr-1 mb-3 mb-xl-0">
                    <button type="button" class="btn btn-primary toolbar-item" data-toggle="modal" data-target="#defaultadd">Add New Contact</button>
                  </div>
                </div>
              </div>

              <!-- Modal -->
              <div class="modal fade text-left" id="defaultadd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog modal-md" role="document">
                  {!! Form::open(['url' => 'addcontact']) !!}
                <div class="modal-content">
                  <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel1">Add Contact</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  </div>
                  <div class="modal-body">
                  <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12">
                  <fieldset class="form-group">
                    <h5>Name</h5>
                      <input type="text" name="contactName" class="form-control" required>
                  </fieldset>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12">
                <fieldset class="form-group">
                  <h5>Location</h5>
                    <input type="text" name="location" class="form-control" required>
                </fieldset>
              </div>
                <div class="col-xl-6 col-lg-6 col-md-6">
                <fieldset class="form-group">
                  <h5>Gender</h5>
                    <select class="custom-select" name="gender" class="form-control" required>
                      <option></option>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                    </select>
                </fieldset>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6">
              <fieldset class="form-group">
                <h5>Date of Birth</h5>
                  <input type="text" name="dob" class="form-control" required>
              </fieldset>
            </div>

                <div class="col-xl-6 col-lg-6 col-md-6">
                  <fieldset class="form-group">
                    <h5>Mobile No.</h5>
                      <input type="number" name="mobileNo" class="form-control" required>
                  </fieldset>
              </div>

              <div class="col-xl-6 col-lg-6 col-md-6">
                <fieldset class="form-group">
                  <h5>Group (Optional)</h5>
                  <select class="custom-select" name="groupId">
                       <option></option>
                       <?php foreach ($groups as $keyf) { ?>
                         <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->groupName; ?></option>
                       <?php } ?>
                     </select>
                </fieldset>
              </div>
              </div>
                  </div>
                  <div class="modal-footer">
                  <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Add</button>
                  </div>
                </div>
                {!! Form::close() !!}
                </div>
              </div>

              <!-- Modal -->
              <div class="modal fade text-left" id="exportdata" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  {!! Form::open(['url' => 'exportcontacts']) !!}
                <div class="modal-content">
                  <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel1">Export Contacts</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  </div>
                  <div class="modal-body">
                  <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12">
                    <fieldset class="form-group">
                      <h5>Group</h5>
                      <select class="custom-select" name="groupId" required>
                           <option value="All">All</option>
                           <option value="CWG">Contacts Without Group</option>
                           <?php foreach ($groups as $keyf) { ?>
                             <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->groupName; ?></option>
                           <?php } ?>
                         </select>
                    </fieldset>
                </div>
                <!-- <div class="col-xl-12 col-lg-12 col-md-12">
                  <fieldset class="form-group">
                    <h5>Export As</h5>
                    <select class="custom-select" name="type" required>
                         <option value="xlsx" selected>XLSX</option>
                       </select>
                  </fieldset>
              </div> -->
              </div>
                  </div>
                  <div class="modal-footer">
                  <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Export</button>
                  </div>
                </div>
                {!! Form::close() !!}
                </div>
              </div>

              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <!-- <h4 class="card-title">Bordered table</h4> -->
                    <div class="table-responsive pt-3">

                        @if (session('status0'))
                          <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          {{ session('status0') }}
                          </div>
                        @endif

                        @if (session('status1'))
                          <div class="alert alert-success alert-dismissible alertbox" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          {{ session('status1') }}
                          </div>
                        @endif

                      <table class="table table-bordered">
                        <thead>
                              <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Mobile No.</th>
                                <th>Gender</th>
                                <th>Date of Birth</th>
                                <th>Location</th>
                                <th>Group</th>
                                <th>Actions</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($contacts as $contact) { ?>
                              <tr>
                                <td><?php echo $contact->id; ?></td>
                                <td><?php echo $contact->contactName; ?></td>
                                <td width="9%"><?php echo $contact->mobileNo; ?></td>
                                <td><?php echo $contact->gender; ?></td>
                                <td><?php echo $contact->dob; ?></td>
                                <td><?php echo $contact->location; ?></td>
                                <td><?php echo $contact->groupName; ?></td>
                                <td width="16.8%">
                                  <?php //if(\App\Userpermissions::checkPermission(17)) { ?>
                                  <a href="<?php $url = URL::to("/viewcontact/".$contact->id); print_r($url); ?>" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> View</a>
                                <?php //} ?>
                                    <?php //if(\App\Userpermissions::checkPermission(8)) { ?>
                                  <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-openedit<?php echo $contact->id; ?>">Edit</button>
                                <?php //} ?>
                                    <?php //if(\App\Userpermissions::checkPermission(9)) { ?>
                                  <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-opendelete<?php echo $contact->id; ?>">Delete</button>
                                <?php //} ?>
                                </td>
                              </tr>

                              <!-- Modal -->
                                                  <div class="modal fade text-left" id="modal-openedit<?php echo $contact->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                      {!! Form::open(['url' => 'editcontact']) !!}
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                      <h4 class="modal-title" id="myModalLabel1">Edit Contact</h4>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                      </button>
                                                      </div>
                                                      <div class="modal-body">
                                                      <div class="row">
                                                      <div class="col-xl-12 col-lg-12 col-md-12">
                                                        <input type="hidden" name="id" value="<?php echo $contact->id; ?>" class="form-control" required>
                                                      <fieldset class="form-group">
                                                        <h5>Name</h5>
                                                          <input type="text" name="contactName" value="<?php echo $contact->contactName; ?>" class="form-control">
                                                      </fieldset>
                                                    </div>

                                                  <div class="col-xl-12 col-lg-12 col-md-12">
                                                  <fieldset class="form-group">
                                                    <h5>Location</h5>
                                                      <input type="text" name="location" value="<?php echo $contact->location; ?>" class="form-control">
                                                  </fieldset>
                                                </div>
                                                <div class="col-xl-6 col-lg-6 col-md-6">
                                                <fieldset class="form-group">
                                                  <h5>Gender</h5>
                                                    <select class="custom-select" name="gender" class="form-control" required>
                                                      <option value="<?php echo $contact->gender; ?>"><?php echo $contact->gender; ?></option>
                                                      <option value="Male">Male</option>
                                                      <option value="Female">Female</option>
                                                    </select>
                                                </fieldset>
                                              </div>
                                              <div class="col-xl-6 col-lg-6 col-md-6">
                                              <fieldset class="form-group">
                                                <h5>Date of Birth</h5>
                                                  <input type="text" name="dob" value="<?php echo $contact->dob; ?>" class="form-control">
                                              </fieldset>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <fieldset class="form-group">
                                                <h5>Mobile No.</h5>
                                                  <input type="number" name="mobileNo" value="<?php echo $contact->mobileNo; ?>" class="form-control" required>
                                              </fieldset>
                                          </div>
                                                  <div class="col-xl-6 col-lg-6 col-md-6">
                                                    <fieldset class="form-group">
                                                      <h5>Group (Optional)</h5>
                                                      <select class="custom-select" name="groupId">
                                                           <option value="<?php echo $contact->groupId; ?>"><?php echo $contact->groupName; ?></option>
                                                           <?php foreach ($groups as $keyf) { ?>
                                                             <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->groupName; ?></option>
                                                           <?php } ?>
                                                         </select>
                                                    </fieldset>
                                                </div>
                                                    </div>
                                                      </div>
                                                      <div class="modal-footer">
                                                      <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                      <button type="submit" class="btn btn-primary">Save Changes</button>
                                                      </div>
                                                    </div>
                                                    {!! Form::close() !!}
                                                    </div>
                                                  </div>


                                                  <!-- Modal -->
                                                              <div class="modal fade text-left" id="modal-opendelete<?php echo $contact->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                  {!! Form::open(['url' => 'deletecontact']) !!}
                                                                <div class="modal-content">
                                                                  <div class="modal-header">
                                                                  <h4 class="modal-title" id="myModalLabel1">Delete Contact</h4>
                                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                  </button>
                                                                  </div>
                                                                  <div class="modal-body">
                                                                  <div class="row">
                                                                  <div class="col-xl-6 col-lg-6 col-md-6">
                                                                    <input type="hidden" name="id" value="<?php echo $contact->id; ?>" class="form-control" required>
                                                                </div>
                                                                <h5>Confirm that you want to delete this record</h5>
                                                              </div>
                                                                  </div>
                                                                  <div class="modal-footer">
                                                                  <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                                  <button type="submit" class="btn btn-primary">Delete</button>
                                                                  </div>
                                                                </div>
                                                                {!! Form::close() !!}
                                                                </div>
                                                              </div>


                              <?php } ?>
                            </tbody>
                      </table>
                      <div class="marginTop-2">
                      {{ $contacts->links() }}
                    </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>



          </div>
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
          @include('footer')
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    @include('footerlinks')
  </body>
</html>
