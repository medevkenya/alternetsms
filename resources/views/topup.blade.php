<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo env("APP_NAME"); ?> | Topup</title>
    @include('headerlinks')
  </head>
  <body>
    @include('promo')
    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      @include('topnav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_settings-panel.html -->
        @include('themesettings')
        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
        @include('sidenav')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold">Topup SMS Units</h3>
              </div>
              <div class="col-sm-6">
                <div class="d-flex align-items-center justify-content-md-end">
                  <!-- <div class="mb-3 mb-xl-0 pr-1">
                      <div class="dropdown">
                        <button class="btn bg-white btn-sm dropdown-toggle btn-icon-text border mr-2" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="typcn typcn-calendar-outline mr-2"></i>Last 7 days
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton3" data-x-placement="top-start">
                          <h6 class="dropdown-header">Last 14 days</h6>
                          <a class="dropdown-item" href="#">Last 21 days</a>
                          <a class="dropdown-item" href="#">Last 28 days</a>
                        </div>
                      </div>
                  </div> -->
                  <!-- <div class="pr-1 mb-3 mr-2 mb-xl-0">
                    <a href="{{URL::to('/topup')}}" class="btn btn-sm bg-white btn-icon-text border"><i class="typcn typcn-arrow-forward-outline mr-2"></i>Topup SMS Units</a>
                  </div> -->
                  <!-- <div class="pr-1 mb-3 mb-xl-0">
                    <a href="{{URL::to('/attendance')}}" class="btn btn-sm bg-white btn-icon-text border"><i class="typcn typcn-info-large-outline mr-2"></i>Attendance</a>
                  </div> -->
                </div>
              </div>
            </div>

            <div class="row" style="margin-top:2%;">

              <div class="col-xl-9 d-flex grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex flex-wrap justify-content-between">
                      <h4 class="card-title mb-3">Payment Instructions</h4>
                      <!-- <a href="{{URL::to('/smshistory')}}">
                        <button type="button" class="btn btn-sm btn-light">View History</button>
                      </a> -->
                    </div>
                    <div class="row">
                      <div class="col-12">

                        <h3>M-Pesa Pay Bill</h3>
                        <ol>
                        <li>Using your MPesa-enabled phone, select "<strong>Pay Bill</strong>" from the M-Pesa menu</li>
                        <li>Enter Africa's Talking Business Number <strong>525900</strong></li>
                        <li>Enter your Africa's Talking Account Number. Your account number is <strong><?php echo $details->accountNo; ?></strong></li>
                        <li>Enter the Amount of credits you want to buy</li>
                        <li>Confirm that all the details are correct and press Ok</li>
                        <li>Check your statement to see your payment. Your API account will also be updated</li>
                        </ol>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 d-flex grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex flex-wrap justify-content-between">
                      <h4 class="card-title mb-3">SMS Units Balance</h4>
                    </div>
                    <div class="row">
                      <div class="col-12">
                        <div class="mb-5">
                          <div class="mr-1">
                            <!-- <div class="text-info mb-1">
                              <a href="{{URL::to('/topup')}}">Topup SMS Units</a>
                            </div> -->
                            <h2 class="mb-2 mt-2 font-weight-bold"><?php echo \App\SMS::getBalance(); ?></h2>
                            <!-- <div class="font-weight-bold">
                              1.4%  Since Last Month
                            </div> -->
                          </div>
                          <!-- <hr>
                          <div class="mr-1">
                            <div class="text-info mb-1">
                              Total Earning
                            </div>
                            <h2 class="mb-2 mt-2  font-weight-bold">87,493</h2>
                            <div class="font-weight-bold">
                              5.43%  Since Last Month
                            </div>
                          </div> -->
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
          @include('footer')
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    @include('footerlinks')

  </body>
</html>
