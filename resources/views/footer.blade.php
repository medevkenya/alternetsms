<footer class="footer">
  <div class="d-sm-flex justify-content-center justify-content-sm-between">
    <span class="text-center text-sm-left d-block d-sm-inline-block">Copyright © <a href="https://www.sms.alternate.co.ke/" target="_blank"><?php echo env("APP_NAME"); ?></a> <?php echo date('Y'); ?></span>
    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">All rights reserved <a href="https://www.alternate.co.ke/" target="_blank">Alternet Limited </a> | Experienced in every kinds of IT Solutions</span>
  </div>
</footer>
