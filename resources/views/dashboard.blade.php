<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo env("APP_NAME"); ?></title>
    @include('headerlinks')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
  </head>
  <body>
    @include('promo')
    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      @include('topnav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_settings-panel.html -->
        @include('themesettings')
        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
        @include('sidenav')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold">Dashboard</h3>
                <p>Your last login: <?php echo \App\User::lastLogin(); ?>.</p>
              </div>
              <div class="col-sm-6">
                <div class="d-flex align-items-center justify-content-md-end">
                  <!-- <div class="mb-3 mb-xl-0 pr-1">
                      <div class="dropdown">
                        <button class="btn bg-white btn-sm dropdown-toggle btn-icon-text border mr-2" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="typcn typcn-calendar-outline mr-2"></i>Last 7 days
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton3" data-x-placement="top-start">
                          <h6 class="dropdown-header">Last 14 days</h6>
                          <a class="dropdown-item" href="#">Last 21 days</a>
                          <a class="dropdown-item" href="#">Last 28 days</a>
                        </div>
                      </div>
                  </div> -->
                  <div class="pr-1 mb-3 mr-2 mb-xl-0">
                    <a href="{{URL::to('/topup')}}" class="btn btn-sm bg-white btn-icon-text border"><i class="typcn typcn-arrow-forward-outline mr-2"></i>Topup SMS Units</a>
                  </div>
                  <div class="pr-1 mb-3 mb-xl-0">
                    <a href="{{URL::to('/attendance')}}" class="btn btn-sm bg-white btn-icon-text border"><i class="typcn typcn-info-large-outline mr-2"></i>Attendance</a>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
  <div class="col-xl-3 d-flex grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="d-flex flex-wrap justify-content-between">
          <h4 class="card-title mb-3">Last 12 Months</h4>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="row">
              <div class="col-sm-12">
                <div class="d-flex justify-content-between mb-4">
                  <div class="font-weight-medium">Month</div>
                  <div class="font-weight-medium">Total</div>
                </div>
                <?php foreach ($monthsdata as $key_month) { ?>
                <div class="d-flex justify-content-between mb-4">
                  <div class="text-secondary font-weight-medium"><?php echo $key_month['month_name']; ?></div>
                  <div class="small"><?php echo $key_month['count']; ?></div>
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-6 d-flex grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="d-flex flex-wrap justify-content-between">
          <h4 class="card-title mb-3">Statistics</h4>
          <a href="{{URL::to('/smshistory')}}">
            <button type="button" class="btn btn-sm btn-light">View History</button>
          </a>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="d-md-flex mb-4">
              <div class="mr-md-5 mb-4">
                <h5 class="mb-1"><i class="typcn typcn-globe-outline mr-1"></i>Groups</h5>
                <h2 class="text-primary mb-1 font-weight-bold"><?php echo \App\Groups::countAll(); ?></h2>
              </div>
              <div class="mr-md-5 mb-4">
                <h5 class="mb-1"><i class="typcn typcn-archive mr-1"></i>Contacts</h5>
                <h2 class="text-secondary mb-1 font-weight-bold"><?php echo \App\Contacts::countAll(); ?></h2>
              </div>
              <div class="mr-md-5 mb-4">
                <h5 class="mb-1"><i class="typcn typcn-tags mr-1"></i>Outbox</h5>
                <h2 class="text-warning mb-1 font-weight-bold"><?php echo \App\Outbox::countAll(); ?></h2>
              </div>
            </div>
            <canvas id="myChart" style="width:100%;max-width:600px"></canvas>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-3 d-flex grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="d-flex flex-wrap justify-content-between">
          <h4 class="card-title mb-3">SMS Units Balance</h4>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="mb-5">
              <div class="mr-1">
                <div class="text-info mb-1">
                  <a href="{{URL::to('/topup')}}">Topup SMS Units</a>
                </div>
                <h2 class="mb-2 mt-2 font-weight-bold"><?php echo \App\SMS::getBalance(); ?></h2>
                <!-- <div class="font-weight-bold">
                  1.4%  Since Last Month
                </div> -->
              </div>
              <!-- <hr>
              <div class="mr-1">
                <div class="text-info mb-1">
                  Total Earning
                </div>
                <h2 class="mb-2 mt-2  font-weight-bold">87,493</h2>
                <div class="font-weight-bold">
                  5.43%  Since Last Month
                </div>
              </div> -->
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

            <div class="row">
              <div class="col-lg-12 d-flex grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex flex-wrap justify-content-between">
                      <h4 class="card-title mb-3">Recent Activities</h4>
                    </div>
                    <div class="row">
                      <div class="col-lg-12">
                        <!-- <div class="d-sm-flex justify-content-between">
                          <div class="dropdown">
                            <button class="btn bg-white btn-sm dropdown-toggle btn-icon-text pl-0" type="button" id="dropdownMenuSizeButton4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Mon,1 Oct 2019 - Tue,2 Oct 2019
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton4" data-x-placement="top-start">
                              <h6 class="dropdown-header">Mon,17 Oct 2019 - Tue,25 Oct 2019</h6>
                              <a class="dropdown-item" href="#">Tue,18 Oct 2019 - Wed,26 Oct 2019</a>
                              <a class="dropdown-item" href="#">Wed,19 Oct 2019 - Thu,26 Oct 2019</a>
                            </div>
                          </div>
                          <div>
                            <button type="button" class="btn btn-sm btn-light mr-2">Day</button>
                            <button type="button" class="btn btn-sm btn-light mr-2">Week</button>
                            <button type="button" class="btn btn-sm btn-light">Month</button>
                          </div>
                        </div> -->
                        <div class="chart-container mt-4">
                          <ul>
                      <?php $activities = \App\Activities::getAll(10); foreach ($activities as $activity) { ?>
                      <li class="timeline-item">
                        <p class="timeline-content"><a href="#"><?php echo $activity->firstName; ?></a> <?php echo $activity->description; ?></p>
                        <p class="event-time"><?php echo $activity->created_at; ?></p>
                      </li>
                      <?php } ?>
                    </ul>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
          @include('footer')
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    @include('footerlinks')

    <script>
    var xValues = [100,200,300,400,500,600,700,800,900,1000];

    new Chart("myChart", {
    type: "line",
    data: {
      labels: xValues,
      datasets: [{
        data: [860,1140,1060,1060,1070,1110,1330,2210,7830,2478],
        borderColor: "red",
        fill: false
      }, {
        data: [1600,1700,1700,1900,2000,2700,4000,5000,6000,7000],
        borderColor: "green",
        fill: false
      }, {
        data: [300,700,2000,5000,6000,4000,2000,1000,200,100],
        borderColor: "blue",
        fill: false
      }]
    },
    options: {
      legend: {display: false}
    }
  });
</script>

    <!-- plugin js for this page -->
    <!-- <script src="{{ URL::asset('assets/vendors/progressbar.js/progressbar.min.js')}}"></script>
    <script src="{{ URL::asset('assets/vendors/chart.js/Chart.min.js')}}"></script> -->
    <!-- End plugin js for this page -->
    <!-- Custom js for this page-->
    <!-- <script src="{{ URL::asset('assets/js/dashboard.js')}}"></script> -->
    <!-- End custom js for this page-->
  </body>
</html>
