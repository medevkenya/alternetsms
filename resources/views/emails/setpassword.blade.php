<!DOCTYPE html>
<html>
<head>
<title>AlternetSMS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif;}

input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

button {
    background-color: #0093DD;
    color: #ffffff;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
	border-radius:1%;
	font-weight:bold;
}

button:hover {
    opacity: 0.8;
}

.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
}

img.avatar {
    width: 40%;
}

.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}


.bcontainer {
	width:30%;
	background: #FFFFFF;
color: #000000;
	padding:1%;
	border-radius:1%;
}
h2 {
text-align: center;
color:#000000;
}
label {
    color:#000000;
}
</style>
</head>
<body>
<div class="bcontainer">
<h2>Recover Password</h2>

  <div class="imgcontainer">
    <img src="{{ URL::to('/') }}/images/logo.png" alt="AlternetSMS" class="avatar">
  </div>

  <p>
    <?php if(isset($status1)) { ?>
  					 <div style="color:green;text-align:center;">
                      <?php echo $status1; //?> Click here to login
					  <a href="{{ url('/signin') }}" style="text-decoration:none;background-color: #225253;
  border: 1px solid #225253;
  color: #fff;
  padding: 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;border-radius:6px;
  margin: 4px 2px;"><b style="color:#fff;">Login Now</b></a>
                      </div>
  				<?php } ?>

  				<?php if(isset($status0)) { ?>
  					 <div style="color:#000000;text-align:center;">
                      <?php echo $status0; ?>
                      </div>
  				<?php } ?>

				<?php if(isset($status10)) { ?>
  					 <div style="color:maroon;text-align:center;">
                      <?php echo $status10; ?>
                      </div>
  				<?php } ?>

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
          @endif

          @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
          @endif
  </p>

  {!! Form::open(['url' => 'DoResetMYPassword']) !!}
  {{ csrf_field() }}
<?php if(isset($status0)) {  } else { ?>
<input type="hidden" name="email" value="<?php echo $email; ?>" required>
  <div class="container">
    <label for="uname"><b>New Password</b></label>
    <input type="password" placeholder="Enter New Password" name="password" required>
    @if ($errors->has('password'))
        <span class="text-danger">{{ $errors->first('password') }}</span>
    @endif

    <label for="psw"><b>Re-Enter Password</b></label>
    <input type="password" placeholder="Re-Enter New Password" name="cpassword" required>
    @if ($errors->has('cpassword'))
        <span class="text-danger">{{ $errors->first('cpassword') }}</span>
    @endif

    <button type="submit">SUBMIT</button>
  </div>
<?php } ?>

{!! Form::close() !!}
</div>
</body>
</html>
