<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<style>
    html, body {font-size: 16px;}
</style>
<table style="background:#00AEED;width:100%;border-radius:4px;padding-left:2%;">
<p style="color:#000000;">
Dear {{ $name }},
</p>
<p style="color:#000000;">
Click on the link below to confirm your email adress and to activate your AlternetSMS Account.
</p>
<br>
<a href="{{$link}}" style="text-decoration:none;background-color: #FFA500;
    color: #000000;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    border-radius: 4px;
    width: 100%;">Activate Account</a>
<br><br>
<p>We request you to add our email to your address book to avoid missing out on information that we send to you from time to time.</p>
<p style="color:#000000;">
Thank you for choosing AlternetSMS.
</p>
<p style="color:#000000;">All emails sent to or from AlternetSMS are subject to AlternetSMS's Terms & Conditions of use.</p>
<p style="color:#000000;">&copy; All rights reserved</p>
</table>
