<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo env("APP_NAME"); ?> | <?php echo $details->contactName; ?> - <?php echo $details->mobileNo; ?></title>
    @include('headerlinks')
    @include('smsheader')
  </head>
  <body>
    @include('promo')
    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      @include('topnav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_settings-panel.html -->
        @include('themesettings')
        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
        @include('sidenav')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold"><?php echo $details->contactName; ?> - <?php echo $details->mobileNo; ?></h3>
                <p>Send message to this member</p>
              </div>

              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <!-- <h4 class="card-title">Bordered table</h4> -->

                      @if (session('status0'))
                      <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      {{ session('status0') }}
                      </div>
                      @endif

                      @if (session('status1'))
                      <div class="alert alert-success alert-dismissible alertbox" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      {{ session('status1') }}
                      </div>
                      @endif

                      {!! Form::open(['url' => 'smsone']) !!}
                         <div class="row">


                                <div class="col-xl-12 col-lg-12 col-md-12">
                                  <fieldset class="form-group">
                                <label for="projectinput9">Main Message</label></br>
                                <div class="result">0 Characters</div> </br></br>
                                <textarea id="message" rows="5" class="form-control" name="message" placeholder="Message..." required></textarea>
                              </fieldset>
                              </div>
                              </div>

                              <div class="form-actions right">
                                          <!-- <button type="reset" class="btn btn-warning mr-1">
                                            <i class="ft-x"></i> Cancel
                                          </button> -->
                                          <button type="submit" class="btn btn-primary">
                                              <i class="fa fa-check-square-o"></i> Send
                                          </button>
                                </div>

                    {!! Form::close() !!}

                  </div>
                </div>
              </div>

              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="table-responsive pt-3">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Group</th>
                          <th>Recipient</th>
                          <th>Message</th>
                          <th>Cost</th>
                          <th>From</th>
                          <th>Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $outboxs = \App\Outbox::getAllByContactId($details->id); foreach ($outboxs as $outbox) { ?>
                        <tr>
                          <td><?php echo $outbox->groupName; ?></td>
                          <td><?php echo $outbox->mobileNo; ?> - <?php echo $outbox->contactName; ?></td>
                          <td><?php echo $outbox->message; ?></td>
                          <td><?php echo $outbox->cost; ?></td>
                          <td><?php echo $outbox->brandName; ?></td>
                          <td><?php echo $outbox->created_at; ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                        </div>
                          <div class="marginTop-2">
                          {{ $outboxs->links() }}
                          </div>
                  </div>
                </div>
              </div>

            </div>



          </div>
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
          @include('footer')
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    @include('footerlinks')
  </body>
</html>
