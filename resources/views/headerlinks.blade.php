<!-- base:css -->
<link rel="stylesheet" href="{{ URL::asset('assets/vendors/typicons.font/font/typicons.css')}}">
<link rel="stylesheet" href="{{ URL::asset('assets/vendors/css/vendor.bundle.base.css')}}">
<!-- endinject -->
<!-- plugin css for this page -->
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet" href="{{ URL::asset('assets/css/vertical-layout-light/style.css')}}">
<!-- endinject -->
<link rel="shortcut icon" href="{{ URL::asset('assets/images/favicon.png')}}" />
