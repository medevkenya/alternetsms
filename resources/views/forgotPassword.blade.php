<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo env("APP_NAME"); ?> | Forgot Password</title>
  @include('headerlinks')
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <img src="{{ URL::asset('images/logo.png')}}" alt="<?php echo env("APP_NAME"); ?>">
              </div>
              <h4>Hello! let's get started</h4>
              <h6 class="font-weight-light">Enter your email to continue.</h6>
              @if (count($errors) > 0)
                 <div class="alert alert-danger">
                     <ul>
                         @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                         @endforeach
                     </ul>
                 </div>
                @endif

                @if ($message = Session::get('error'))
                     <div class="alert alert-danger">
                         {{ $message }}
                     </div>
                @endif

                @if ($message = Session::get('success'))
                     <div class="alert alert-success">
                         {{ $message }}
                     </div>
                @endif

               {!! Form::open(['url'=>'resetPassword','class'=>'pt-3']) !!}
               {{ csrf_field() }}
                <div class="form-group">
                  <input type="email" name="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Email Address" required>
                  @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                      @endif
                </div>
                <div class="mt-3">
                  <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">RECOVER PASSWORD</button>
                </div>

                <div class="text-center mt-4 font-weight-light">
                  Remembered the password? <a href="{{URL::to('/')}}" class="text-primary">Sign In</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  @include('footerlinks')
  <!-- endinject -->
</body>

</html>
