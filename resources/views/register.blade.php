<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo env("APP_NAME"); ?> | Create Account</title>
  @include('headerlinks')
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-8 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <img src="{{ URL::asset('images/logo.png')}}" alt="<?php echo env("APP_NAME"); ?>">
              </div>
              <h4>Hello! let's get started</h4>
              <h6 class="font-weight-light">Fill your details to continue.</h6>
              @if (count($errors) > 0)
                 <div class="alert alert-danger">
                     <ul>
                         @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                         @endforeach
                     </ul>
                 </div>
                @endif

                @if ($message = Session::get('error'))
                     <div class="alert alert-danger">
                         {{ $message }}
                     </div>
                @endif

                @if ($message = Session::get('success'))
                     <div class="alert alert-success">
                         {{ $message }}
                     </div>
                @endif

               {!! Form::open(['url'=>'doRegister','class'=>'pt-3']) !!}
               {{ csrf_field() }}

               <div class="row">

               <div class="form-group col-md-6">
                 <input type="text" name="firstName" class="form-control form-control-lg" id="firstName" placeholder="Your First Name" required>
                 @if ($errors->has('firstName'))
                       <span class="text-danger">{{ $errors->first('firstName') }}</span>
                     @endif
               </div>

               <div class="form-group col-md-6">
                 <input type="text" name="lastName" class="form-control form-control-lg" id="lastName" placeholder="Your Last Name" required>
                 @if ($errors->has('lastName'))
                       <span class="text-danger">{{ $errors->first('lastName') }}</span>
                     @endif
               </div>

               <div class="form-group col-md-6">
                 <input type="number" name="mobileNo" class="form-control form-control-lg" id="mobileNo" placeholder="Your Mobile No." required>
                 @if ($errors->has('mobileNo'))
                       <span class="text-danger">{{ $errors->first('mobileNo') }}</span>
                     @endif
               </div>

               <div class="form-group col-md-6">
                 <input type="text" name="organizationName" class="form-control form-control-lg" id="organizationName" placeholder="Organization Name" required>
                 @if ($errors->has('organizationName'))
                       <span class="text-danger">{{ $errors->first('organizationName') }}</span>
                     @endif
               </div>

               <div class="form-group col-md-6">
                 <input type="text" name="location" class="form-control form-control-lg" id="location" placeholder="Organization Location" required>
                 @if ($errors->has('location'))
                       <span class="text-danger">{{ $errors->first('location') }}</span>
                     @endif
               </div>

                <div class="form-group col-md-6">
                  <input type="email" name="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Email Address" required>
                  @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                      @endif
                </div>

                <div class="form-group col-md-6">
                  <input type="password" name="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password" required>
                  @if ($errors->has('password'))
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                      @endif
                </div>

                <div class="form-group col-md-6">
                  <input type="password" name="cpassword" class="form-control form-control-lg" id="cpassword" placeholder="Repeat Password" required>
                  @if ($errors->has('cpassword'))
                        <span class="text-danger">{{ $errors->first('cpassword') }}</span>
                      @endif
                </div>

                <div class="mt-3 col-md-6">
                  <p><a href="{{URL::to('/terms')}}" class="auth-link text-black">By creating an account you agree to our fair terms of use?</a></p>
                  <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">CREATE ACCOUNT</button>
                </div>

              </div>

                <!-- <div class="my-2 d-flex justify-content-between align-items-center">
                  <div class="form-check">
                    <label class="form-check-label text-muted">
                      <input type="checkbox" class="form-check-input">
                      Keep me signed in
                    </label>
                  </div>
                  <a href="{{URL::to('/terms')}}" class="auth-link text-black">By creating an account you agree to our terms of use?</a>
                </div> -->
                <!-- <div class="mb-2">
                  <button type="button" class="btn btn-block btn-facebook auth-form-btn">
                    <i class="typcn typcn-social-facebook-circular mr-2"></i>Connect using facebook
                  </button>
                </div> -->
                <div class="text-center mt-4 font-weight-light">
                  You already have an account? <a href="{{URL::to('/')}}" class="text-primary">Sign In</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  @include('footerlinks')
  <!-- endinject -->
</body>

</html>
