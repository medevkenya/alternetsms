<nav class="sidebar sidebar-offcanvas" id="sidebar">
<ul class="nav">
  <li class="nav-item">
    <div class="d-flex sidebar-profile">
      <div class="sidebar-profile-image">
        <img src="{{ URL::asset('profiles/profile.png')}}" alt="image">
        <span class="sidebar-status-indicator"></span>
      </div>
      <div class="sidebar-profile-name">
        <p class="sidebar-name">
          {{Auth::user()->firstName}} {{Auth::user()->lastName}}
        </p>
        <p class="sidebar-designation">
          Welcome
        </p>
      </div>
    </div>
    <div class="nav-search">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Type to search..." aria-label="search" aria-describedby="search">
        <div class="input-group-append">
          <span class="input-group-text" id="search">
            <i class="typcn typcn-zoom"></i>
          </span>
        </div>
      </div>
    </div>
    <p class="sidebar-menu-title">Dash menu</p>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{URL::to('/dashboard')}}">
      <i class="typcn typcn-device-desktop menu-icon"></i>
      <span class="menu-title">Dashboard <span class="badge badge-primary ml-3">New</span></span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
      <i class="typcn typcn-briefcase menu-icon"></i>
      <span class="menu-title">Groups</span>
      <i class="typcn typcn-chevron-right menu-arrow"></i>
    </a>
    <div class="collapse" id="ui-basic">
      <ul class="nav flex-column sub-menu">
        <li class="nav-item"> <a class="nav-link" href="{{URL::to('/groups')}}">Manage Groups</a></li>
      </ul>
    </div>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
      <i class="typcn typcn-film menu-icon"></i>
      <span class="menu-title">Contacts</span>
      <i class="menu-arrow"></i>
    </a>
    <div class="collapse" id="form-elements">
      <ul class="nav flex-column sub-menu">
        <li class="nav-item"><a class="nav-link" href="{{URL::to('/contacts')}}">Manage Contacts</a></li>
      </ul>
    </div>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#charts" aria-expanded="false" aria-controls="charts">
      <i class="typcn typcn-chart-pie-outline menu-icon"></i>
      <span class="menu-title">SMS Centre</span>
      <i class="menu-arrow"></i>
    </a>
    <div class="collapse" id="charts">
      <ul class="nav flex-column sub-menu">
        <li class="nav-item"> <a class="nav-link" href="{{URL::to('/sendtosingle')}}">Send to Individual</a></li>
        <li class="nav-item"> <a class="nav-link" href="{{URL::to('/sendtogroup')}}">Send to Group</a></li>
        <li class="nav-item"> <a class="nav-link" href="{{URL::to('/sendtoexcel')}}">Send to Excel List</a></li>
        <li class="nav-item"> <a class="nav-link" href="{{URL::to('/smsbatch')}}">Manage SMS Batch</a></li>
        <li class="nav-item"> <a class="nav-link" href="{{URL::to('/outbox')}}">History</a></li>
      </ul>
    </div>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{URL::to('/topup')}}">
      <i class="typcn typcn-document-text menu-icon"></i>
      <span class="menu-title">Topup SMS Units</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#form-attendance" aria-expanded="false" aria-controls="form-attendance">
      <i class="typcn typcn-document menu-icon"></i>
      <span class="menu-title">Attendance</span>
      <i class="menu-arrow"></i>
    </a>
    <div class="collapse" id="form-attendance">
      <ul class="nav flex-column sub-menu">
        <!-- <li class="nav-item"><a class="nav-link" href="{{URL::to('/members')}}">Membership</a></li> -->
        <li class="nav-item"><a class="nav-link" href="{{URL::to('/attendance')}}">Manage Attendance</a></li>
        <li class="nav-item"><a class="nav-link" href="{{URL::to('/attendancelist')}}">Attendance List</a></li>
      </ul>
    </div>
  </li>
</ul>
<ul class="sidebar-legend">
  <li>
    <p class="sidebar-menu-title">Category</p>
  </li>
  <li class="nav-item"><a href="#" class="nav-link">#Sales</a></li>
  <li class="nav-item"><a href="#" class="nav-link">#Marketing</a></li>
  <li class="nav-item"><a href="#" class="nav-link">#Growth</a></li>
</ul>
</nav>
