<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo env("APP_NAME"); ?></title>
    @include('headerlinks')
    @include('smsheader')
  </head>
  <body>
    @include('promo')
    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      @include('topnav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_settings-panel.html -->
        @include('themesettings')
        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
        @include('sidenav')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold">Send SMS to excel list</h3>
                <p>Compose your SMS and send instantly</p>
              </div>
              <!-- <div class="col-sm-6">
                <div class="d-flex align-items-center justify-content-md-end">
                  <div class="mb-3 mb-xl-0 pr-1">
                      <div class="dropdown">
                        <button class="btn bg-white btn-sm dropdown-toggle btn-icon-text border mr-2" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="typcn typcn-calendar-outline mr-2"></i>Last 7 days
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton3" data-x-placement="top-start">
                          <h6 class="dropdown-header">Last 14 days</h6>
                          <a class="dropdown-item" href="#">Last 21 days</a>
                          <a class="dropdown-item" href="#">Last 28 days</a>
                        </div>
                      </div>
                  </div>
                  <div class="pr-1 mb-3 mr-2 mb-xl-0">
                    <button type="button" class="btn btn-sm bg-white btn-icon-text border"><i class="typcn typcn-arrow-forward-outline mr-2"></i>Export</button>
                  </div>
                  <div class="pr-1 mb-3 mb-xl-0">
                    <button type="button" class="btn btn-sm bg-white btn-icon-text border"><i class="typcn typcn-info-large-outline mr-2"></i>info</button>
                  </div>
                </div>
              </div> -->

              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <!-- <h4 class="card-title">Bordered table</h4> -->
                    <!-- <div class="table-responsive pt-3"> -->

                      @if (session('status0'))
                        <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ session('status0') }}
                        </div>
                        @endif

                        @if (session('status1'))
                        <div class="alert alert-success alert-dismissible alertbox" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ session('status1') }}
                        </div>
                        @endif

                        {!! Form::open(array('route' => 'smsexcel','method'=>'POST','files'=>'true')) !!}
                          @csrf
                           <div class="row">

                          <div class="col-xl-4 col-lg-4 col-md-4">
                            <fieldset class="form-group">
                              <h5>Browse Excel File</h5>
                              <input type="file" name="file" class="form-control" required>
                            </fieldset>
                        </div>

                        <div class="col-xl-4 col-lg-4 col-md-4">
                          <fieldset class="form-group">
                            <h5>Scheduled Date</h5>
                            <input type="date" id="date" class="form-control"  placeholder="mm-dd-yyyy" value="<?php date_default_timezone_set("Africa/Nairobi"); echo date('Y-m-d');?>" name="send_date" required>
                          </fieldset>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4">
                        <fieldset class="form-group">
                          <h5>Scheduled Time</h5>
                          <input type="text" id="time" class="form-control"  placeholder="eg 22:50" value="<?php echo date('H:i');?>" name="send_time" required>
                        </fieldset>
                    </div>

                                  <div class="col-xl-12 col-lg-12 col-md-12">
                                    <fieldset class="form-group">
                                  <h5>Main Message</h5></br>
                                  <div class="result">0 Characters</div> </br>
                                  <textarea id="message" rows="5" class="form-control" name="message" placeholder="Message..." required></textarea>
                                  </fieldset>
                                </div>



                                </div>

                                <div class="form-actions right">
                                            <!-- <button type="reset" class="btn btn-warning mr-1">
                                              <i class="ft-x"></i> Cancel
                                            </button> -->
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-check-square-o"></i> Send
                                            </button>
                                  </div>

                      {!! Form::close() !!}

                      <div class="row">
                      <div class="col-md-8 grid-margin" style="margin-top:4%;">
                    <p>Please organize your Excel contacts like the format below.</p>
                    <img src="{{ URL::asset('images/excel.png')}}" style="min-width:100%;" alt="banner-img" class="img-fluid">
                    </div>
                    <div class="col-md-4 grid-margin" style="margin-top:4%;">
                    <a href="{{ URL::asset('public/images/Book2.csv')}}" class="btn btn-primary btn-block"><i class="fa fa-eye"></i> Download Template</a>
                  </div>
                  </div>

                    <!-- </div> -->
                  </div>
                </div>
              </div>

            </div>



          </div>
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
          @include('footer')
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    @include('footerlinks')
  </body>
</html>
