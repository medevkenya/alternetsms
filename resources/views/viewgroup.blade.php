<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo env("APP_NAME"); ?> | <?php echo $details->groupName; ?></title>
    @include('headerlinks')
    @include('smsheader')
  </head>
  <body>
    @include('promo')
    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      @include('topnav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_settings-panel.html -->
        @include('themesettings')
        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
        @include('sidenav')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold"><?php echo $details->groupName; ?></h3>
                <p>List of members in this group</p>
              </div>
              <div class="col-sm-6">
                <div class="d-flex align-items-center justify-content-md-end">
                  <div class="mb-3 mb-xl-0 pr-1">
                      <div class="dropdown">
                        <button class="btn bg-white btn-sm dropdown-toggle btn-icon-text border mr-2" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="typcn typcn-calendar-outline mr-2"></i>Export Members
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton3" data-x-placement="top-start">
                          <!-- <h6 class="dropdown-header">Last 14 days</h6> -->
                          <a class="dropdown-item" href="<?php $url = URL::to("/exportcontacts/xlsx"); print_r($url); ?>">Export as XLSX</a>
                          <a class="dropdown-item" href="<?php $url = URL::to("/exportcontacts/csv"); print_r($url); ?>">Export as CSV</a>
                        </div>
                      </div>
                  </div>
                  <div class="pr-1 mb-3 mb-xl-0">
                    <button type="button" class="btn btn-primary toolbar-item" data-toggle="modal" data-target="#defaultadd">Add New Member</button>
                    <!-- <button type="button" class="btn btn-sm bg-white btn-icon-text border"><i class="typcn typcn-arrow-forward-outline mr-2"></i>Export</button> -->
                  </div>
                  <!-- <div class="pr-1 mb-3 mb-xl-0">
                    <button type="button" class="btn btn-sm bg-white btn-icon-text border"><i class="typcn typcn-info-large-outline mr-2"></i>info</button>
                  </div> -->
                </div>
              </div>

              <!-- Modal -->
              <div class="modal fade text-left" id="defaultadd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  {!! Form::open(['url' => 'addcontact']) !!}
                <div class="modal-content">
                  <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel1">Add Member</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  </div>
                  <div class="modal-body">
                  <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12">
                  <fieldset class="form-group">
                    <h5>Name (optional)</h5>
                      <input type="text" name="contactName" class="form-control">
                  </fieldset>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12">
                  <fieldset class="form-group">
                    <h5>Mobile No.</h5>
                      <input type="number" name="mobileNo" class="form-control" required>
                  </fieldset>
              </div>
              <div class="col-xl-12 col-lg-12 col-md-12">
              <fieldset class="form-group">
                <h5>Location</h5>
                  <input type="text" name="location" class="form-control" required>
              </fieldset>
            </div>
              <input type="hidden" name="groupId" value="<?php echo $details->id; ?>">
              </div>
                  </div>
                  <div class="modal-footer">
                  <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Add</button>
                  </div>
                </div>
                {!! Form::close() !!}
                </div>
              </div>

              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <!-- <h4 class="card-title">Bordered table</h4> -->

                      @if (session('status0'))
                      <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      {{ session('status0') }}
                      </div>
                      @endif

                      @if (session('status1'))
                      <div class="alert alert-success alert-dismissible alertbox" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      {{ session('status1') }}
                      </div>
                      @endif

                        {!! Form::open(['url' => 'smsgroup']) !!}
                         <div class="row">
                           <input type="hidden" name="groupId" value="<?php echo $details->id; ?>">
                          <!-- <div class="col-xl-4 col-lg-4 col-md-4">
                            <fieldset class="form-group">
                              <h5>Brand Name</h5>
                              <select class="form-control" name="senderId">
                                   <option></option>
                                   <?php //$brandnames = \App\Brandnames::getAll(); foreach ($brandnames as $brandname) { ?>
                                     <option value="<?php //echo $brandname->id; ?>"><?php //echo $brandname->brandName; ?></option>
                                   <?php //} ?>
                                 </select>
                            </fieldset>
                        </div> -->

                        <!-- <div class="col-xl-4 col-lg-4 col-md-4">
                          <fieldset class="form-group">
                            <h5>Group</h5>
                            <select class="form-control" name="groupId" disabled>
                                   <option value="<?php //echo $details->id; ?>"><?php //echo $details->groupName; ?></option>
                               </select>
                          </fieldset>
                      </div>
                      <br> -->
                                <div class="col-xl-12 col-lg-12 col-md-12">
                                  <fieldset class="form-group">
                                <label for="projectinput9">Main Message</label>
                              </br>
                                <div class="result">0 Characters</div> </br>
                                <textarea id="message" rows="5" class="form-control" name="message" placeholder="Message...Note: 1 message is equal to 160 characters" required></textarea>
                                </fieldset>
                              </div>
                              </div>

                              <div class="form-actions right">
                                          <!-- <button type="reset" class="btn btn-warning mr-1">
                                            <i class="ft-x"></i> Cancel
                                          </button> -->
                                          <button type="submit" class="btn btn-primary">
                                              <i class="fa fa-check-square-o"></i> Send
                                          </button>
                                </div>

                    {!! Form::close() !!}

                  </div>
                </div>
              </div>

              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="table-responsive pt-3">
                    <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>Name</th>
                                <th>Mobile No.</th>
                                <th>Group</th>
                                <th>Actions</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($contacts as $contact) { ?>
                              <tr>
                                <td><?php echo $contact->contactName; ?></td>
                                <td><?php echo $contact->mobileNo; ?></td>
                                <td><?php echo $contact->groupName; ?></td>
                                <td>
                                  <a href="<?php $url = URL::to("/viewmember/".$contact->id); print_r($url); ?>" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> View</a>
                                  <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-openedit<?php echo $contact->id; ?>">Edit</button>
                                  <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-opendelete<?php echo $contact->id; ?>">Delete</button>
                                </td>
                              </tr>

                              <!-- Modal -->
                                                  <div class="modal fade text-left" id="modal-openedit<?php echo $contact->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                      {!! Form::open(['url' => 'editcontact']) !!}
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                      <h4 class="modal-title" id="myModalLabel1">Edit Member</h4>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                      </button>
                                                      </div>
                                                      <div class="modal-body">
                                                      <div class="row">
                                                      <div class="col-xl-12 col-lg-12 col-md-12">
                                                        <input type="hidden" name="id" value="<?php echo $contact->id; ?>" class="form-control" required>
                                                      <fieldset class="form-group">
                                                        <h5>Name (Optional)</h5>
                                                          <input type="text" name="contactName" value="<?php echo $contact->contactName; ?>" class="form-control">
                                                      </fieldset>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12">
                                                      <fieldset class="form-group">
                                                        <h5>Mobile No.</h5>
                                                          <input type="number" name="mobileNo" value="<?php echo $contact->mobileNo; ?>" class="form-control" required>
                                                      </fieldset>
                                                  </div>
                                                  <div class="col-xl-12 col-lg-12 col-md-12">
                                                    <fieldset class="form-group">
                                                      <h5>Group (Optional)</h5>
                                                      <select class="form-control" name="groupId">
                                                           <option value="<?php echo $contact->groupId; ?>"><?php echo $contact->groupName; ?></option>
                                                           <?php foreach ($groups as $keyf) { ?>
                                                             <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->groupName; ?></option>
                                                           <?php } ?>
                                                         </select>
                                                    </fieldset>
                                                </div>
                                                    </div>
                                                      </div>
                                                      <div class="modal-footer">
                                                      <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                      <button type="submit" class="btn btn-primary">Save Changes</button>
                                                      </div>
                                                    </div>
                                                    {!! Form::close() !!}
                                                    </div>
                                                  </div>


                                                  <!-- Modal -->
                                                              <div class="modal fade text-left" id="modal-opendelete<?php echo $contact->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                  {!! Form::open(['url' => 'deletecontact']) !!}
                                                                <div class="modal-content">
                                                                  <div class="modal-header">
                                                                  <h4 class="modal-title" id="myModalLabel1">Delete Member</h4>
                                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                  </button>
                                                                  </div>
                                                                  <div class="modal-body">
                                                                  <div class="row">
                                                                  <div class="col-xl-6 col-lg-6 col-md-6">
                                                                    <input type="hidden" name="id" value="<?php echo $contact->id; ?>" class="form-control" required>
                                                                </div>
                                                                <h5>Confirm that you want to delete this record</h5>
                                                              </div>
                                                                  </div>
                                                                  <div class="modal-footer">
                                                                  <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                                  <button type="submit" class="btn btn-primary">Delete</button>
                                                                  </div>
                                                                </div>
                                                                {!! Form::close() !!}
                                                                </div>
                                                              </div>


                              <?php } ?>
                            </tbody>
                          </table>
                        </div>
                          <div class="marginTop-2">
                          {{ $contacts->links() }}
                          </div>
                  </div>
                </div>
              </div>

            </div>



          </div>
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
          @include('footer')
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    @include('footerlinks')
  </body>
</html>
