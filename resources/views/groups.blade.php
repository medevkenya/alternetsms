<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo env("APP_NAME"); ?></title>
    @include('headerlinks')
  </head>
  <body>
    @include('promo')
    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      @include('topnav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_settings-panel.html -->
        @include('themesettings')
        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
        @include('sidenav')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold">Manage Groups</h3>
                <p>List of all your groups</p>
              </div>
              <div class="col-sm-6">
                <div class="d-flex align-items-center justify-content-md-end">
                  <div class="mb-3 mb-xl-0 pr-1">
                      <div class="dropdown">
                        <button class="btn bg-white btn-sm dropdown-toggle btn-icon-text border mr-2" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="typcn typcn-calendar-outline mr-2"></i>Export Groups
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton3" data-x-placement="top-start">
                          <!-- <h6 class="dropdown-header">Last 14 days</h6> -->
                          <a class="dropdown-item" href="<?php $url = URL::to("/exportgroups/xlsx"); print_r($url); ?>">Export as XLSX</a>
                          <a class="dropdown-item" href="<?php $url = URL::to("/exportgroups/csv"); print_r($url); ?>">Export as CSV</a>
                        </div>
                      </div>
                  </div>
                  <div class="pr-1 mb-3 mb-xl-0">
                    <button type="button" class="btn btn-primary toolbar-item" data-toggle="modal" data-target="#defaultadd">Add New Group</button>
                    <!-- <button type="button" class="btn btn-sm bg-white btn-icon-text border"><i class="typcn typcn-arrow-forward-outline mr-2"></i>Export</button> -->
                  </div>
                  <!-- <div class="pr-1 mb-3 mb-xl-0">
                    <button type="button" class="btn btn-sm bg-white btn-icon-text border"><i class="typcn typcn-info-large-outline mr-2"></i>info</button>
                  </div> -->
                </div>
              </div>

              <!-- Modal -->
              <div class="modal fade text-left" id="defaultadd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  {!! Form::open(['url' => 'addgroup']) !!}
                <div class="modal-content">
                  <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel1">Add Group</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  </div>
                  <div class="modal-body">
                  <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12">
                  <fieldset class="form-group position-relative has-icon-left">
                    <h5>Group Name</h5>
                      <input type="text" name="groupName" class="form-control" required>
                  </fieldset>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12">
                <fieldset class="form-group position-relative has-icon-left">
                  <h5>Description</h5>
                    <textarea rows="6" name="description" class="form-control" required></textarea>
                </fieldset>
              </div>
              </div>
                  </div>
                  <div class="modal-footer">
                  <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Add</button>
                  </div>
                </div>
                {!! Form::close() !!}
                </div>
              </div>

              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <!-- <h4 class="card-title">Bordered table</h4> -->
                    <div class="table-responsive pt-3">

                        @if (session('status0'))
                          <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          {{ session('status0') }}
                          </div>
                        @endif

                        @if (session('status1'))
                          <div class="alert alert-success alert-dismissible alertbox" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          {{ session('status1') }}
                          </div>
                        @endif

                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Group Name</th>
                            <th>Description</th>
                            <th>Contacts</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $groups = \App\Groups::getAll(0); foreach ($groups as $group) { ?>
                              <tr>
                                <td><?php echo $group->groupName; ?></td>
                                <td><?php echo $group->description; ?></td>
                                <td width="5%"><?php echo \App\Contacts::countByGroup($group->id); ?></td>
                                <td width="16.8%">
                                  <?php //if(\App\Userpermissions::checkPermission(16)) { ?>
                                  <a href="<?php $url = URL::to("/viewgroup/".$group->id); print_r($url); ?>" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> View</a>
                                <?php //} ?>
                                    <?php //if(\App\Userpermissions::checkPermission(5)) { ?>
                                  <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-openedit<?php echo $group->id; ?>">Edit</button>
                                <?php //} ?>
                                    <?php //if(\App\Userpermissions::checkPermission(6)) { ?>
                                  <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-opendelete<?php echo $group->id; ?>">Delete</button>
                                <?php //} ?>
                                </td>
                              </tr>

                              <!-- Modal -->
                              <div class="modal fade text-left" id="modal-openedit<?php echo $group->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                {!! Form::open(['url' => 'editgroup']) !!}
                                <div class="modal-content">
                                  <div class="modal-header">
                                  <h4 class="modal-title" id="myModalLabel1">Edit Group</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                  </div>
                                  <div class="modal-body">
                                  <div class="row">
                                  <div class="col-xl-12 col-lg-12 col-md-12">
                                    <input type="hidden" name="id" value="<?php echo $group->id; ?>" class="form-control" required>
                                  <fieldset class="form-group position-relative has-icon-left">
                                    <h5>Group Name</h5>
                                      <input type="text" name="groupName" value="<?php echo $group->groupName; ?>" class="form-control" required>
                                  </fieldset>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12">
                                <fieldset class="form-group position-relative has-icon-left">
                                  <h5>Description</h5>
                                    <textarea rows="6" name="description" class="form-control" required><?php echo $group->description; ?></textarea>
                                </fieldset>
                              </div>
                                </div>
                                  </div>
                                  <div class="modal-footer">
                                  <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-primary">Save Changes</button>
                                  </div>
                                </div>
                                {!! Form::close() !!}
                                </div>
                              </div>

                              <!-- Modal -->
                              <div class="modal fade text-left" id="modal-opendelete<?php echo $group->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  {!! Form::open(['url' => 'deletegroup']) !!}
                                <div class="modal-content">
                                  <div class="modal-header">
                                  <h4 class="modal-title" id="myModalLabel1">Delete Group</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                  </div>
                                  <div class="modal-body">
                                  <div class="row">
                                  <div class="col-xl-6 col-lg-6 col-md-6">
                                    <input type="hidden" name="id" value="<?php echo $group->id; ?>" class="form-control" required>
                                </div>
                                <h5>Confirm that you want to delete this record</h5>
                              </div>
                                  </div>
                                  <div class="modal-footer">
                                  <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-primary">Delete</button>
                                  </div>
                                </div>
                                {!! Form::close() !!}
                                </div>
                              </div>

                              <?php } ?>
                        </tbody>
                      </table>
                      <div class="marginTop-2">
                      {{ $groups->links() }}
                    </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>



          </div>
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
          @include('footer')
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    @include('footerlinks')
  </body>
</html>
