<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@signin');
Route::get('/signin', 'HomeController@signin');
Route::post('dosignin', 'HomeController@dosignin');
Route::get('login', [ 'as' => 'login', 'uses' => 'HomeController@signin']);
Route::get('/register', 'HomeController@register');
Route::post('doRegister', 'HomeController@doRegister');
Route::get('/logout', 'HomeController@logout');
Route::get('/activateAccount', 'HomeController@activateAccount');

Route::get('ResetPasswordLink/{PasswordConfirmation}/{email}', 'ForgotpasswordController@resetPasswordLink');
Route::get('forgotPassword', 'ForgotpasswordController@forgotPassword');
Route::post('resetPassword', 'ForgotpasswordController@resetPassword');
Route::post('DoResetMYPassword', 'ForgotpasswordController@doResetPassword');

Route::get('resendRecoveryEmail/{email}', 'ForgotpasswordController@resendRecoveryEmail');

Route::get('/terms', 'SiteController@terms');
Route::get('/privacy', 'SiteController@privacy');
Route::get('/help', 'SiteController@help');

Route::group(['middleware' => 'auth'], function () {

Route::get('dashboard', 'AccountController@dashboard');

Route::get('settings', 'AccountController@settings');

Route::get('help', 'AccountController@help');

Route::get('profile', 'AccountController@profile');
Route::post('updateProfile', 'AccountController@updateProfile');

Route::get('changepassword', 'AccountController@changepassword');
Route::post('doChangePass', 'AccountController@doChangePass');

Route::get('changePic', 'AccountController@changePic');
Route::post('UpdatePic', 'AccountController@UpdatePic')->name('UpdatePic');

Route::get('activities', 'AccountController@activities');
Route::get('topup', 'AccountController@topup');
Route::get('attendance', 'AccountController@attendance');
Route::get('attendancelist', 'AccountController@attendancelist');

Route::post('addattendance', 'AccountController@addattendance');
Route::post('editattendance', 'AccountController@editattendance');
Route::post('deleteattendance', 'AccountController@deleteattendance');
Route::post('searchattendance', 'AccountController@searchattendance');
Route::get('searchattendance', 'AccountController@attendancelist');

Route::post('addsession', 'AccountController@addsession');
Route::post('editsession', 'AccountController@editsession');
Route::post('deletesession', 'AccountController@deletesession');

Route::get('groups', 'GroupsController@groups');
Route::post('addgroup', 'GroupsController@addgroup');
Route::post('deletegroup', 'GroupsController@deletegroup');
Route::post('editgroup', 'GroupsController@editgroup');
Route::get('exportgroups/{type}', 'GroupsController@exportgroups')->name('exportgroups');
Route::get('viewgroup/{id}', 'GroupsController@viewgroup');

Route::get('contacts', 'ContactsController@contacts');
Route::post('addcontact', 'ContactsController@addcontact');
Route::post('deletecontact', 'ContactsController@deletecontact');
Route::post('editcontact', 'ContactsController@editcontact');
Route::post('exportcontacts', 'ContactsController@exportcontacts')->name('exportcontacts');
Route::get('viewcontact/{id}', 'ContactsController@viewcontact');

Route::get('outbox', 'OutboxController@outbox');

Route::get('sendtosingle', 'SMSController@sendtosingle');
Route::get('sendtogroup', 'SMSController@sendtogroup');
Route::get('sendtoexcel', 'SMSController@sendtoexcel');

Route::get('getDownload', 'SMSController@getDownload');

Route::get('smsbatch', 'SMSController@smsbatch');
Route::post('editbatch', 'SMSController@editbatch');
Route::post('deletebatch', 'SMSController@deletebatch');

Route::post('smsone', 'SMSController@smsone');
Route::post('smsgroup', 'SMSController@smsgroup');
Route::post('smsexcel',array('as'=>'smsexcel','uses'=>'SMSController@smsexcel'));

Route::get('subscriptions', 'SubscriptionsController@subscriptions');
// Route::get('settings', 'SettingsController@settings');
Route::post('addbrandname', 'SettingsController@addbrandname');
Route::post('deletebrandname', 'SettingsController@deletebrandname');
Route::post('editbrandname', 'SettingsController@editbrandname');

Route::get('roles', 'RolesController@roles');
Route::post('addrole', 'RolesController@addrole');
Route::post('deleterole', 'RolesController@deleterole');
Route::post('editrole', 'RolesController@editrole');

Route::get('users', 'UsersController@users');
Route::post('adduser', 'UsersController@adduser');
Route::post('deleteuser', 'UsersController@deleteuser');
Route::post('edituser', 'UsersController@edituser');

Route::get('permissions', 'PermissionsController@permissions');
Route::get('updatepermission', 'PermissionsController@updatepermission');

Route::post('flutterwaveverify', 'FlutterwaveController@flutterwaveverify')->name('flutterwaveverify');
Route::post('flutterwaveinitiate', 'FlutterwaveController@flutterwaveinitiate')->name('flutterwaveinitiate');

});

//Cron
Route::get('cronsms', 'CronsController@cronsms');
