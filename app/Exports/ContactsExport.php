<?php
namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Contacts;
use Auth;

class ContactsExport implements FromCollection, WithHeadings
{

  protected $groupId;

    public function __construct(String $groupId) {

        $this->groupId = $groupId;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
      if($this->groupId=="All") {
        return Contacts::select('contacts.contactName','contacts.mobileNo','contacts.dob','contacts.gender','contacts.location','groups.groupName')
        ->leftJoin('groups','contacts.groupId','=','groups.id')
        ->where('contacts.adminId',Auth::user()->adminId)
        ->where('contacts.isDeleted', 0)
        ->orderBy('contacts.id','DESC')
        ->get();
      }
      else if($this->groupId=="CWG") {
        return Contacts::select('contacts.contactName','contacts.mobileNo','contacts.dob','contacts.gender','contacts.location','groups.groupName')
        ->leftJoin('groups','contacts.groupId','=','groups.id')
        ->where('contacts.adminId',Auth::user()->adminId)
        ->where('contacts.groupId',null)
        ->where('contacts.isDeleted', 0)
        ->orderBy('contacts.id','DESC')
        ->get();
      }
      else {
        return Contacts::select('contacts.contactName','contacts.mobileNo','contacts.dob','contacts.gender','contacts.location','groups.groupName')
        ->leftJoin('groups','contacts.groupId','=','groups.id')
        ->where('contacts.adminId',Auth::user()->adminId)
        ->where('contacts.groupId',$this->groupId)
        ->where('contacts.isDeleted', 0)
        ->orderBy('contacts.id','DESC')
        ->get();
      }

    }

    public function headings(): array
    {
        return [
            'Name',
            'Mobile No.',
            'Date of Birth',
            'Gender',
            'Location',
            'Group'
        ];
    }

}
