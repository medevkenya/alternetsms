<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;
use App\Outbox;
use App\User;
use DB;

class Contacts extends Model
{
    protected $table = 'contacts';
    protected $guarded = ['contactName'];

    public static function exportContacts()
    {
      if($groupId=="All") {
        $list = Contacts::getAll();
      }
      else if($groupId=="CWG") {
        $list = Contacts::select('contacts.*','groups.groupName')
        ->leftJoin('groups','contacts.groupId','=','groups.id')->where('contacts.adminId',$adminId)
        ->where('contacts.groupId',null)->where('contacts.isDeleted', 0)->orderBy('contacts.id','DESC')->get();
      }
      else {
        $list = Contacts::select('contacts.*','groups.groupName')
        ->leftJoin('groups','contacts.groupId','=','groups.id')->where('contacts.adminId',$adminId)
        ->where('contacts.groupId',$groupId)->where('contacts.isDeleted', 0)->orderBy('contacts.id','DESC')->get();
      }
    }

    public static function getAll() {
      return Contacts::select('contacts.*','groups.groupName')
      ->leftJoin('groups','contacts.groupId','=','groups.id')->where('contacts.adminId',Auth::user()->adminId)
      ->where('contacts.isDeleted', 0)->orderBy('contacts.id','DESC')->paginate(50);
    }

    public static function getAllByGroup($groupId) {
      return Contacts::select('contacts.*','groups.groupName')
      ->leftJoin('groups','contacts.groupId','=','groups.id')
      ->where('contacts.groupId',$groupId)
      ->where('contacts.adminId',Auth::user()->adminId)
      ->where('contacts.isDeleted', 0)
      ->orderBy('contacts.id','DESC')->paginate(50);
    }

    public static function checkSize($groupId)
    {
      return Contacts::where('groupId',$groupId)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->count();
    }


    public static function countAll()
    {
        return Contacts::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
    }

    public static function countAllGeneral()
    {
        return Contacts::where('isDeleted',0)->count();
    }

    public static function countByGroup($id)
    {
        return Contacts::where('groupId',$id)->where('isDeleted',0)->count();
    }

    public static function getAllByGroupId($id)
    {
        return Contacts::where('groupId',$id)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();
    }

    public static function storeone($contactName,$mobileNo,$groupId,$location,$gender,$dob)
    {
        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Contacts;
        $model->contactName = $contactName;
        $model->mobileNo = "254".substr($mobileNo, -9);
        $model->groupId = $groupId;
        $model->location = $location;
        $model->gender = $gender;
        $model->dob = $dob;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model) {
            Activities::saveLog("Added new contact [".$mobileNo."]");
            return true;
        }
        return false;
    }

    public static function updateone($id, $contactName,$mobileNo,$groupId,$location,$gender,$dob)
    {
        $model = Contacts::find($id);
        $model->contactName = $contactName;
        $model->mobileNo = "254".substr($mobileNo, -9);
        $model->groupId = $groupId;
        $model->location = $location;
        $model->gender = $gender;
        $model->dob = $dob;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited contact [".$mobileNo."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Contacts::find($id);

        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted contact [".$id."]");
            return true;
        }
        return false;
    }

    public static function logMessages($ref_queue_id,$message,$senderId,$groupId,$mobileNo=0) {

      $adminId = Auth::user()->adminId;

      $stringLength = strlen($message);
			if($stringLength > 160)
			{
				$stringLength = ceil($stringLength / 160);
			}
			else
			{
				$stringLength = 1;
			}

			$cost = ($stringLength * env("averageSMSCost")) * 1;

      if($groupId == 0) {
        $model = new Outbox;
        $model->senderId = $senderId;
        $model->mobileNo = "254".substr($mobileNo, -9);
        $model->message = $message;
        $model->cost = $cost;
        $model->adminId = $adminId;
        //$model->created_by = $created_by;
        $model->save();
      }
      else {

      $contacts = self::getAllByGroupId($groupId);

      $total_contacts=count($contacts);
                 $max_mem_size=1000;//10000;//to get from db as a config

                if($total_contacts){

                	$iterations=ceil(($total_contacts)/$max_mem_size);

                	if($total_contacts>$max_mem_size){
						$st=0;
	                	$stp= $max_mem_size;
                	}else{
                		$st=0;
                		$stp=$total_contacts-1;

                	}
                	//log::info('start- stop:'.$start." and ".$stop."  iter:".$iterations);


                	for ($k=1; $k<=$iterations ; $k++) { //first for loop

                		$arr=[];

	                	for ($i=$st; $i <=$stp; $i++) {


			            $h = ['mobileNo' => $contacts[$i]->mobileNo,'ref_queue_id'=>$ref_queue_id,'adminId'=>$adminId,'senderId'=>$senderId,'contactId'=>$contacts[$i]->id,'groupId'=>$groupId,'message'=>$message,'cost'=>$cost];

					     array_push($arr, $h);

	                	}


//return $arr;
	                	//DB::table($request->group_id)->insert($arr);
	                	//DB::table('messages')->insert($arr);

	                	//log::info('start1:'.$st." stp1:".$stp."  i:".$k);

		                    $st=$stp+1;
		                	if($k==($iterations-1)){
		                		$stp= ($total_contacts)-1;
		                	}else{
		                		$stp=$st+$max_mem_size;
		                	}


		            //log::info('start2:'.$start."stp2: ".$stop."  i:".$k);

                	}

                }

if(!empty($arr)){
            DB::table('outbox')->insert($arr);
                        //dd('Insert Record successfully.');
						//DB::table('ref_queue')->where('ref_queue_id', $ref_queue_id)->update(['batch_size' => count($contacts)]);
						//$msg="SMS batch has been created and queued successfully.";
					//	return Redirect::to('groupcontacts/'.$group_id)->with(['status1'=>$msg]);

     }

   }

    }


}
