<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Modules extends Model
{
    protected $table = 'modules';

    public static function getAll() {
        return Modules::where('isDeleted',0)->get();
    }

}
