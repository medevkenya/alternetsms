<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Brandnames extends Model
{
    protected $table = 'brandnames';

    public static function checkMinimumLimit($senderId)
    {
        $check = Brandnames::where('id',$senderId)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->first();
        return $check->balanceLimit;
    }

    public static function getAll()
    {
        return Brandnames::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();
    }

    public static function storeone($brandName,$username,$apikey,$balanceLimit)
    {
        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Brandnames;
        $model->brandName = $brandName;
        $model->username = $username;
        $model->balanceLimit = $balanceLimit;
        $model->apikey = $apikey;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model) {
            Activities::saveLog("Added new SMS Brand Name [".$brandName."]");
            return true;
        }
        return false;
    }

    public static function updateone($id, $brandName,$username,$apikey,$balanceLimit)
    {
        $model = Brandnames::find($id);
        $model->brandName = $brandName;
        $model->username = $username;
        $model->balanceLimit = $balanceLimit;
        $model->apikey = $apikey;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited SMS brand name [".$brandName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Brandnames::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted brand name [".$id."]");
            return true;
        }
        return false;
    }

}
