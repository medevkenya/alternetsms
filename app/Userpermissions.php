<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use App\Permissions;

class Userpermissions extends Model
{
    protected $table = 'userpermissions';

    public static function getAll($roleId)
    {
        return Userpermissions::select('userpermissions.*','permissions.permissionName')
        ->leftJoin('permissions','userpermissions.permissionId','=','permissions.id')
        ->where('userpermissions.adminId',Auth::user()->adminId)->where('userpermissions.roleId',$roleId)->where('userpermissions.isDeleted',0)->get();
    }

    public static function seedPermissions($roleId)
    {
        //$arr=array();
        $arr2=[];
        $allpermissions = Permissions::where('isDeleted',0)->get();
        // foreach ($allpermissions as $keyp)
        // {
        //   $arr[] = array('id'=>$keyp->id);
        // }

        //$permissions = Permissions::where('isDeleted',0)->get();
        foreach ($allpermissions as $key)
        {
          // DB::table('userpermissions')->insert([
          // 'roleId' => $roleId,
          // 'permissionId' => $key['id'],
          // 'adminId' => Auth::user()->adminId,
          // 'created_by' => Auth::user()->id
          // ]);
          $h = [
          'roleId' => $roleId,
          'permissionId' => $key->id,
          'adminId' => Auth::user()->adminId,
          'created_by' => Auth::user()->id
        ];
        array_push($arr2, $h);
          // $model = new Userpermissions;
          // $model->roleId = $roleId;
          // $model->permissionId = $key['id'];
          // $model->adminId = Auth::user()->adminId;
          // $model->created_by = Auth::user()->id;
          // $model->save();
        }

        DB::table('userpermissions')->insert($arr2);

        //return true;
    }

    public static function deletePermissions($roleId)
    {
        $permissions = Userpermissions::where('roleId',$roleId)->where('isDeleted',0)->get();
        foreach ($permissions as $key) {
          $model = Userpermissions::find($key->id);
          $model->isDeleted = 1;
          $model->status = 0;
          $model->save();
        }
        return true;
    }

    public static function checkPermission($id)
    {
      return Userpermissions::where('roleId',Auth::user()->roleId)
      ->where('adminId',Auth::user()->adminId)
      ->where('permissionId',$id)
      ->where('status',1)
      ->where('isDeleted',0)->first();
    }

    public static function checkPermissionRole($roleId,$permissionId)
    {
      return Userpermissions::where('roleId',$roleId)
      ->where('permissionId',$permissionId)
      ->where('isDeleted',0)->first();
    }

}
