<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use App\Subscriptions;
use Log;

class Payments extends Model
{

    protected $table = 'payments';

    public static function initiatePayment($amount,$packageId) {
      $userId	= Auth::user()->id;

      $ii= strtoupper(str_shuffle(time().$userId.$packageId."abcdefghijklmnopqrstuvwxyz"));
      $reference = substr($ii, -6);

      $model = new Payments;
      $model->userId = $userId;
      $model->amount = $amount;
      $model->packageId = $packageId;
      $model->reference = $reference;
      $model->currency = "KES";
      $model->save();
      if($model) {
        return array('billRefNumber'=>$reference,'public_key'=>env("flutterwave_public_key"));
      }
      else {
        return null;
      }
    }

    /**
    * Action: Process webhook payload
    */
    public static function processWebhook($txref,$fee,$transaction_id,$details)
    {

      $result = Payments::updatePaymentStatus($txref,$fee,$transaction_id,$details);
      if($result)
      {

        return array("ResultCode"=>0,"ResultDesc"=>"Transaction was successful");

      }
      else
      {

        return array("ResultCode"=>1,"ResultDesc"=>"Transaction was successful but pay and subscription not updated.");

      }

    }

    /**
    * Action: Process payment verification
    */
    public static function verifyPayment($txref,$transaction_id)
    {

      $paydetails = Payments::where('reference',$txref)->where('status',0)->first();
      if(!empty($paydetails)) {

      $amount = $paydetails->amount; //Correct Amount from DB
      $currency = $paydetails->currency; //Correct Currency from DB

      $query = array(
          "SECKEY" => env("flutterwave_secret_key"),
          "txref" => $txref
      );

      $data_string = json_encode($query);

      $ch = curl_init('https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/verify');
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

      $response = curl_exec($ch);

      $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
      $header = substr($response, 0, $header_size);
      $body = substr($response, $header_size);

      curl_close($ch);

      $resp = json_decode($response, true);

      log::info("Flutterwave verifyPayment response ".json_encode($resp));

      $paymentStatus = $resp['status'];

      if ($paymentStatus == "success") {

        $chargeResponsecode = $resp['data']['chargecode'];
        $chargeAmount = $resp['data']['amount'];
        $chargeCurrency = $resp['data']['currency'];

      if (($chargeResponsecode == "00" || $chargeResponsecode == "0") && ($chargeAmount == $amount)  && ($chargeCurrency == $currency))
      {
        // transaction was successful...
        log::info("Transaction was successful. currency --".$currency."-- Charge Response Code --".$chargeResponsecode."-- amount --".$amount."-- Payment Status --".$paymentStatus);
        // please check other things like whether you already gave value for this ref
        // if the email matches the customer who owns the product etc
        //Give Value and return to Success page
        $charged_amount = $resp['data']['chargedamount'];
        $fee = $resp['data']['appfee'];//$charged_amount - $chargeAmount;
        $result = Payments::updatePaymentStatus($txref,$fee,$transaction_id,$response);
        if($result)
        {

        return array("ResultCode"=>0,"ResultDesc"=>"Transaction was successful");

      }
      else
      {

        return array("ResultCode"=>1,"ResultDesc"=>"Transaction was successful but pay and subscription not updated.");

      }
      }
      else
      {
          //Error occurred. Dont Give Value and return to Failure page
          log::info("Transaction failed. currency --".$currency."-- Charge Response Code --".$chargeResponsecode."-- amount --".$amount."-- Payment Status --".$paymentStatus." Reason: ".$resp['data']['message']);
          return array("ResultCode"=>1,"ResultDesc"=>"Transaction failed - Reason: ".$resp['data']['message']);

      }

    }
    else {
      //Error occurred. Dont Give Value and return to Failure page
      log::info("Transaction failed. Payment Status --".$paymentStatus." Reason: ".$resp['data']['message']);
      return array("ResultCode"=>1,"ResultDesc"=>"Transaction failed - Reason: ".$resp['data']['message']);

    }

    }
    else
    {

      //Dont Give Value and return to Failure page
      log::info("Deposit record with reference --".$txref."-- not found");
      return array("ResultCode"=>1,"ResultDesc"=>"Transaction failed. Deposit record ref: ( ".$txref." ) not found");

    }

    }

    public static function updatePaymentStatus($reference,$fee,$transaction_id,$details) {
      $res = Payments::where('reference',$reference)
      ->update(['fee'=>$fee,'status'=>1,'transaction_id'=>$transaction_id,'details'=>$details]);
      if($res) {
        Subscriptions::updateDates($reference,$transaction_id);
        return true;
      }
      else {
        return false;
      }
    }

}
