<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class Attendance extends Model
{
    protected $table = 'attendance_list';

    public static function add($all)
   {
    $model = new Attendance;
    $model->sessionId = $all['sessionId'];
    $model->fullName = $all['fullName'];
    $model->mobileNo = $all['mobileNo'];
    $model->location = $all['location'];
    $model->attendance_date = $all['date'];
    $model->created_by = Auth::user()->id;
    $model->save();
    if($model) {
      return true;
    }
    else {
      return false;
    }
  }

  public static function edit($all)
 {
  $model = Attendance::find($all['id']);
  $model->sessionId = $all['sessionId'];
  $model->fullName = $all['fullName'];
  $model->mobileNo = $all['mobileNo'];
  $model->location = $all['location'];
  $model->attendance_date = $all['date'];
  $model->save();
  if($model) {
    return true;
  }
  else {
    return false;
  }
}

  public static function deleteattendance($all)
  {
    $model = Attendance::find($all['id']);
    $model->isDeleted = 1;
    $model->save();
    if($model) {
      return true;
    }
    else {
      return false;
    }
  }


    public static function getAllByMonth() {
      $items = Attendance::select(
                    DB::raw("(COUNT(*)) as count"),
                    DB::raw("MONTHNAME(created_at) as month_name")
                )->whereYear('created_at', date('Y'))
                ->groupBy('month_name')
                ->get()
                ->toArray();
                if(empty($items)) {
                  $items = array(
                    array('month_name'=>'January','count'=>"0 - Not Set"),
                    array('month_name'=>'February','count'=>"0 - Not Set"),
                    array('month_name'=>'March','count'=>"0 - Not Set"),
                    array('month_name'=>'April','count'=>"0 - Not Set"),
                    array('month_name'=>'May','count'=>"0 - Not Set"),
                    array('month_name'=>'June','count'=>"0 - Not Set"),
                    array('month_name'=>'July','count'=>"0 - Not Set"),
                    array('month_name'=>'August','count'=>"0 - Not Set"),
                    array('month_name'=>'September','count'=>"0 - Not Set"),
                    array('month_name'=>'October','count'=>"0 - Not Set"),
                    array('month_name'=>'November','count'=>"0 - Not Set"),
                    array('month_name'=>'December','count'=>"0 - Not Set")
                  );
                }
                return $items;
    }

    public static function getAll() {
        return Attendance::select('attendance_list.*','sessions_list.name as sessionName')
        ->leftJoin('sessions_list','attendance_list.sessionId','=','sessions_list.id')
        ->where('attendance_list.isDeleted',0)
        ->orderBy('attendance_list.id','DESC')
        ->paginate(100);
    }

    public static function getAllBySearch($sessionId,$date) {
      if(!empty($sessionId)) {
        return Attendance::select('attendance_list.*','sessions_list.name as sessionName')
        ->leftJoin('sessions_list','attendance_list.sessionId','=','sessions_list.id')
        ->where('attendance_list.attendance_date',$date)
        ->where('attendance_list.sessionId',$sessionId)
        ->where('attendance_list.isDeleted',0)
        ->orderBy('attendance_list.id','DESC')
        ->paginate(100);
      }
      else {
        return Attendance::select('attendance_list.*','sessions_list.name as sessionName')
        ->leftJoin('sessions_list','attendance_list.sessionId','=','sessions_list.id')
        ->where('attendance_list.attendance_date',$date)
        ->where('attendance_list.isDeleted',0)
        ->orderBy('attendance_list.id','DESC')
        ->paginate(100);
      }
    }

}
