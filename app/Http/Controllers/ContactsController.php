<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Contacts;
use App\Groups;
use App\Logs;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ContactsExport;

class ContactsController extends Controller
{

  public function exportcontacts(Request $request)
  {

         $groupId = $request->groupId;

         return Excel::download(new ContactsExport($groupId), 'contacts.xlsx');
         //return (new ContactsExport($groupId))->download('contacts.xlsx');

        //  $list=[];
        //
        //  if($groupId=="All") {
        //    $list = Contacts::getAll();
        //  }
        //  else if($groupId=="CWG") {
        //    $list = Contacts::select('contacts.*','groups.groupName')
        //    ->leftJoin('groups','contacts.groupId','=','groups.id')->where('contacts.adminId',$adminId)
        //    ->where('contacts.groupId',null)->where('contacts.isDeleted', 0)->orderBy('contacts.id','DESC')->get();
        //  }
        //  else {
        //    $list = Contacts::select('contacts.*','groups.groupName')
        //    ->leftJoin('groups','contacts.groupId','=','groups.id')->where('contacts.adminId',$adminId)
        //    ->where('contacts.groupId',$groupId)->where('contacts.isDeleted', 0)->orderBy('contacts.id','DESC')->get();
        //  }
        //
        //  $temp=[];
        //  foreach ($list  as $key => $value) {
        //    $h=['Group'=>$value->groupName,'Name'=>$value->contactName,'Mobile No.'=>$value->mobileNo];
        //    array_push($temp, $h);
        //  }
        //
        // return \Excel::create('Exported_Contacts', function($excel) use ($temp) {
        //     $excel->sheet('sheet phone', function($sheet) use ($temp)
        //     {
        //         $sheet->fromArray($temp);
        //     });
        // })->download($type);
        //
        // return Excel::download($data, 'contacts.xlsx');
  }

    public function contacts()
    {
        $adminId	= Auth::user()->adminId;
        $contacts = Contacts::getAll();
        $groups = Groups::getAll(10000);
        return view('contacts', ['contacts'=>$contacts,'groups'=>$groups]);
    }

    public function viewcontact($id)
    {
        $adminId	= Auth::user()->adminId;
        $details = Contacts::where('id',$id)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->first();
        if($details) {
            return view('viewcontact', ['details'=>$details]);
        }
        else {
            return Redirect::back()->with(['status0'=>'Contact not found']);
        }
    }

    public function addcontact(Request $request)
    {
        $contactName = $request->contactName;
        $groupId = $request->groupId;
        $mobileNo = $request->mobileNo;
        $location = $request->location;
        $gender = $request->gender;
        $dob = $request->dob;
        $adminId	= Auth::user()->adminId;

        $groupstatusc = Contacts::where('contactName', $contactName)->where('mobileNo', $mobileNo)
        ->where('groupId', $groupId)->where('adminId',$adminId)->where('isDeleted', 0)->first();
        if ($groupstatusc) {
            return Redirect::back()->with(['status0'=>'Contact already exists.']);
        } else {
            $add = Contacts::storeone($contactName,$mobileNo,$groupId,$location,$gender,$dob);
            if ($add) {
                return Redirect::back()->with(['status1'=>'New record was created successfully.']);
            } else {
                return Redirect::back()->with(['status0'=>'Error occurred while creating record.']);
            }
        }
    }


    public function editcontact(Request $request)
    {
        $id = $request->id;
        $contactName = $request->contactName;
        $groupId = $request->groupId;
        $mobileNo = $request->mobileNo;
        $location = $request->location;
        $gender = $request->gender;
        $dob = $request->dob;

            $update = Contacts::updateone($id, $contactName, $mobileNo, $groupId,$location,$gender,$dob);
            if ($update) {
                return Redirect::back()->with(['status1'=>'The record was updated successfully']);
            } else {
                return Redirect::back()->with(['status0'=>'Error occurred while updating record']);
            }

    }


    public function deletecontact(Request $request)
    {
        $id = $request->id;
        $delete = Contacts::deleteone($id);
        if ($delete) {
            return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
        } else {
            return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
        }
    }
}
