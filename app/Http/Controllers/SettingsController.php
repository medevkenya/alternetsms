<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Settings;
use App\Brandnames;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class SettingsController extends Controller {

	public function settings()
	{
		$list = Brandnames::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
		return view('settings',['list'=>$list]);
	}

	public function addbrandname(Request $request)
	{
			$brandName = $request->brandName;
			$username = $request->username;
			$apikey = $request->apikey;
			$balanceLimit = $request->balanceLimit;
			$adminId	= Auth::user()->adminId;

			$check = Brandnames::where('brandName', $brandName)->where('apikey', $apikey)
			->where('username', $username)->where('adminId',$adminId)->where('isDeleted', 0)->first();
			if ($check) {
					return Redirect::back()->with(['status0'=>'Record already exists.']);
			} else {
					$add = Brandnames::storeone($brandName,$apikey,$username,$balanceLimit);
					if ($add) {
							return Redirect::back()->with(['status1'=>'New record was created successfully.']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while creating record.']);
					}
			}
	}


	public function editbrandname(Request $request)
	{
			$id = $request->id;
			$brandName = $request->brandName;
			$username = $request->username;
			$apikey = $request->apikey;
			$balanceLimit = $request->balanceLimit;

					$update = Brandnames::updateone($id, $brandName, $apikey, $username,$balanceLimit);
					if ($update) {
							return Redirect::back()->with(['status1'=>'The record was updated successfully']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while updating record']);
					}

	}


	public function deletebrandname(Request $request)
	{
			$id = $request->id;
			$delete = Brandnames::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
