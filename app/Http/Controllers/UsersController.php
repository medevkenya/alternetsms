<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Settings;
use App\Roles;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class UsersController extends Controller {

	public function users()
	{
		$list = User::select('users.*','roles.roleName')
		->leftJoin('roles','users.roleId','=','roles.id')
		->where('users.adminId',Auth::user()->adminId)
		->where('users.isDeleted',0)
		->orderBy('id','DESC')->get();
		$roles = Roles::getAll();
		return view('users',['list'=>$list,'roles'=>$roles]);
	}

	public function adduser(Request $request)
	{
			$firstName = $request->firstName;
			$lastName = $request->lastName;
			$email = $request->email;
			$roleId = $request->roleId;
			$password = $request->password;
			$adminId	= Auth::user()->adminId;

			$check = User::where('email', $email)->where('adminId',$adminId)->where('isDeleted', 0)->first();
			if ($check) {
					return Redirect::back()->with(['status0'=>'Record already exists.']);
			} else {
					$add = User::storeone($firstName,$lastName,$email,$password,$roleId);
					if ($add) {
							return Redirect::back()->with(['status1'=>'New record was created successfully.']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while creating record.']);
					}
			}
	}


	public function edituser(Request $request)
	{
			$id = $request->id;
			$firstName = $request->firstName;
			$lastName = $request->lastName;
			$email = $request->email;
			$roleId = $request->roleId;

					$update = User::updateone($id, $firstName, $lastName,$email, $roleId);
					if ($update) {
							return Redirect::back()->with(['status1'=>'The record was updated successfully']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while updating record']);
					}

	}


	public function deleteuser(Request $request)
	{
			$id = $request->id;
			$delete = User::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
