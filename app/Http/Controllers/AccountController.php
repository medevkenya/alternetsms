<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Activities;
use App\Outbox;
use App\Brandnames;
use App\Groups;
use App\Attendance;
use App\Sessionslist;
use Mail;
use Hash;
use Auth;
use Illuminate\Support\Facades\Vallogidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Log;
use Carbon\Carbon;

class AccountController extends Controller
{

    public function dashboard()
    {
        $monthsdata = Outbox::getAllByMonth();
        return view('dashboard',['monthsdata'=>$monthsdata]);
    }

    public function profile()
    {
      $userid	= Auth::user()->id;
      $details = User::where('id',$userid)->first();
      return view('profile',['details'=>$details]);
    }

    public function activities()
    {
      return view('activities');
    }

    public function help()
    {
      return view('help');
    }

    public function settings()
    {
      $details = Brandnames::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->first();
      return view('settings',['details'=>$details]);
    }

    public function topup()
    {
      $details = Brandnames::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->first();
      return view('topup',['details'=>$details]);
    }

    public function attendance()
    {
      //$groups = Groups::getAll(0);
      $monthsdata = Attendance::getAllByMonth();
      return view('attendance', ['monthsdata'=>$monthsdata]);
    }

    public function attendancelist()
    {
      $sessionlist = Sessionslist::getAll();
      $list = Attendance::getAll();
      return view('attendancelist', ['sessionlist'=>$sessionlist,'list'=>$list]);
    }

    public function searchattendance()
    {

      request()->validate([
          'sessionId' => 'nullable',
          'date' => 'required',
      ], [
          'date.required' => 'Date is required',
      ]);

      $sessionlist = Sessionslist::getAll();
      $list = Attendance::getAllBySearch(request()->sessionId,request()->date);
      return view('attendancelist', ['sessionlist'=>$sessionlist,'list'=>$list]);
    }

    public function addattendance(Request $request)
    {

        request()->validate([
            'sessionId' => 'nullable',
            'fullName' => 'required',
            'mobileNo' => 'required',
            'location' => 'required',
            'date' => 'required',
        ], [
            'name.required' => 'Full name is required',
        ]);

        $check = Attendance::where('sessionId',request()->sessionId)->where('fullName',request()->fullName)->where('mobileNo',request()->mobileNo)->where('attendance_date',request()->date)->where('isDeleted',0)->first();
        if($check) {
          return redirect()->back()->with('error', 'Attendee already exists for this session');
        }

        $res = Attendance::add(request()->all());
        if($res) {
          return redirect()->back()->with('success', 'Attendee was added successfully');
        }
        else {
          return redirect()->back()->with('error', 'Adding attendee failed. Please try again');
        }

    }

    public function editattendance(Request $request)
    {

        request()->validate([
            'sessionId' => 'nullable',
            'id' => 'required',
            'fullName' => 'required',
            'mobileNo' => 'required',
            'location' => 'required',
            'date' => 'required',
        ], [
            'name.required' => 'Full name is required',
        ]);

        $check = Attendance::where('sessionId',request()->sessionId)->where('fullName',request()->fullName)->where('mobileNo',request()->mobileNo)->where('attendance_date',request()->date)->where('id','!=',request()->id)->where('isDeleted',0)->first();
        if($check) {
          return redirect()->back()->with('error', 'Attendee already exists for this session');
        }

        $res = Attendance::edit(request()->all());
        if($res) {
          return redirect()->back()->with('success', 'Attendee was updated successfully');
        }
        else {
          return redirect()->back()->with('error', 'Updating attendee failed. Please try again');
        }

    }

    public function deleteattendance(Request $request)
    {

        request()->validate([
            'id' => 'required',
        ], [
            'id.required' => 'Id is required',
        ]);

        $res = Attendance::deleteattendance(request()->all());
        if($res) {
          return redirect()->back()->with('success', 'Attendance was deleted successfully');
        }
        else {
          return redirect()->back()->with('error', 'Deleting attendance failed. Please try again');
        }

    }

    public function addsession(Request $request)
    {

        request()->validate([
            'name' => 'required',
            'description' => 'required',
        ], [
            'name.required' => 'Full name is required',
        ]);

        $check = Sessionslist::where('name',request()->name)->where('isDeleted',0)->first();
        if($check) {
          return redirect()->back()->with('error', 'Session name already exists');
        }

        $res = Sessionslist::add(request()->all());
        if($res) {
          return redirect()->back()->with('success', 'Session was added successfully');
        }
        else {
          return redirect()->back()->with('error', 'Adding session failed. Please try again');
        }

    }

    public function editsession(Request $request)
    {

        request()->validate([
            'id' => 'required',
            'name' => 'required',
            'description' => 'required',
        ], [
            'name.required' => 'Session name is required',
        ]);

        $check = Sessionslist::where('name',request()->name)->where('id','!=',request()->id)->where('isDeleted',0)->first();
        if($check) {
          return redirect()->back()->with('error', 'Session name already exists');
        }

        $res = Sessionslist::edit(request()->all());
        if($res) {
          return redirect()->back()->with('success', 'Session was updated successfully');
        }
        else {
          return redirect()->back()->with('error', 'Updating session failed. Please try again');
        }

    }

    public function deletesession(Request $request)
    {

        request()->validate([
            'id' => 'required',
        ], [
            'id.required' => 'Id is required',
        ]);

        $res = Sessionslist::deletesession(request()->all());
        if($res) {
          return redirect()->back()->with('success', 'Session was deleted successfully');
        }
        else {
          return redirect()->back()->with('error', 'Deleting session failed. Please try again');
        }

    }

    public function changepassword()
    {
        return view('changepassword');
    }

    public function doChangePass()
    {
        $oldpassword	= Input::get('oldpassword');
        $password	= Input::get('password');
        $cpassword 	= Input::get('cpassword');
        $userid	= Auth::user()->id;

        $oldhashedPassword = Hash::make($oldpassword);

        $checkold = User::where('id', '=', $userid)->first();
        if (!Hash::check(Input::get('oldpassword'), $checkold->password)) {
            return redirect()->back()->withInput($request->input())->with('error', 'Current password is incorrect. Please check and try again');
        } elseif (strlen($password) < 6) {
            return redirect()->back()->withInput($request->input())->with('error', 'Password must be at least 6 characters');
        } elseif ($password != $cpassword) {
            return redirect()->back()->withInput($request->input())->with('error', 'Password and Repeat Password do not match');
        } else {

            $hashedPassword = Hash::make($password);

            $edituserpass = User::where('id', $userid)->update(['password' => $hashedPassword]);
            if ($edituserpass) {
                return redirect()->back()->withInput($request->input())->with('success', 'Your account password was changed successfully');
            } else {
                return redirect()->back()->withInput($request->input())->with('error', 'Failed to change account password, try again');
            }
        }
    }


    public function updateProfile(Request $request)
    {
      //https://stackoverflow.com/questions/51962486/datepicker-set-maximum-date-to-18-years-ago-from-current-date
        request()->validate([
            'firstName' => 'required|min:3|max:50',
            'lastName' => 'required|min:3|max:50',
            'mobileNo' => 'required',// input starts with 01 and is followed by 9 numbers
            'location' => 'required',
            // 'dob' => 'required|date_format:d-m-Y|before:18 years',
            //'terms' => 'accepted',
        ], [
            'firstName.required' => 'First Name is required',
            'firstName.min' => 'First Name must be at least 3 characters.',
            'firstName.max' => 'First Name should not be greater than 20 characters.',
            'lastName.required' => 'Last Name is required',
            'lastName.min' => 'Last Name must be at least 3 characters.',
            'lastName.max' => 'Last Name should not be greater than 20 characters.',
            'mobileNo.required' => 'Mobile Number is required',
            'mobileNo.regex' => 'Mobile number should be 10 digits starting with 07',
            //'about.required' => 'Provide brief description about yourself',
            //'terms.required' => 'Please accept our terms of service',
        ]);

        // $input = request()->except('password','confirm_password');
        // $user=new User($input);//
        // $user->password=bcrypt(request()->password);
        // $user->save();
        //request()->firstName,request()->lastName,request()->mobileNo,,request()->password
        $res = User::editMyProfile(request()->all());
        if($res) {
          return redirect()->back()->withInput($request->input())->with('success', 'Your profile was updated successfully');
        }
        else {
          return redirect()->back()->withInput($request->input())->with('error', 'Failed to save. Please try again');
        }

    }

    public function changePic()
    {
      $userid	= Auth::user()->id;
      $details = User::where('id',$userid)->first();
      return view('changePic',['details'=>$details]);
    }

    public function UpdatePic()
    {

  // getting all of the post data
        $id	= Auth::user()->id;
        $file = array('image' => Input::file('profilePic'));

        // setting up rules
        //  $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000 required|image|mimes:jpeg,png,jpg,gif,svg|max:2048
        $rules = array('image' => 'required|image|mimes:jpeg,png,jpg,gif|max:5048',);
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);

        // checking file is valid.
        if (Input::file('profilePic')->isValid()) {
            //$destinationPath = 'otheruploads'; // upload path
            $destinationPath = public_path('/images');

            $extension = Input::file('profilePic')->getClientOriginalExtension(); // getting image extension

            $fileName = rand(11111, 99999).'.'.$extension; // renameing image

            Input::file('profilePic')->move($destinationPath, $fileName); // uploading file to given path

            //file name to take to db
            $profilePic=  $fileName;

            User::where('id', $id)->update(['profilePic' => $profilePic]);

            return Redirect::to('changePic')->with(['status1'=>'Your profile picture was updated successfully.']);
        } else {
            return Redirect::to('changePic')->with(['status0'=>'Sorry, error occurred while updating your profile picture. Try again.']);
        }
    }

    public function ErrorPage()
    {
        $datee = date('Y-m-d');
        return view('error');
    }

}
