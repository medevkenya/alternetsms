<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Outbox;
use App\SMS;
use App\Groups;
use App\Settings;
use App\Members;
use App\Brandnames;
use Response;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\MembersImport;

class SMSController extends Controller {

	public function getDownload(){

        $file = public_path()."/images/Book2.csv";
        $headers = array('Content-Type: application/vnd.ms-excel',);
        return Response::download($file, 'AlternetSMS_Book2.csv',$headers);
    }

	public function sendtosingle()
	{
		return view('sendtosingle');
	}

	public function sendtogroup()
	{
		return view('sendtogroup');
	}

	public function sendtoexcel()
	{
		return view('sendtoexcel');
	}

	public function smsbatch()
	{
		return view('smsbatch');
	}

	public function editbatch(Request $request)
	{

			$id = $request->id;
			$senderId = $request->senderId;
			$groupId = $request->groupId;
			$message = $request->message;
			$send_date = $request->send_date;
			$send_time = $request->send_time;
			$overall_status = $request->overall_status;

					$update = SMS::updateone($id,$senderId,$groupId,$message,$send_date,$send_time,$overall_status);
					if ($update) {
							return Redirect::back()->with(['status1'=>'The batch was updated successfully']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while updating batch']);
					}

	}

	public function deletebatch(Request $request)
	{
			$id = $request->id;
			$delete = SMS::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Batch was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting batch']);
			}
	}

	public function smsone(Request $request)
	{
			//$senderId = $request->senderId;
			$message = $request->message;
			$mobileNo = "254".substr($request->mobileNo, -9);
			$adminId	= Auth::user()->adminId;

			$send_date = $request->send_date;
			$send_time = $request->send_time;

			$stringLength = strlen($message);
			if($stringLength > 160)
			{
				$stringLength = ceil($stringLength / 160);
			}
			else
			{
				$stringLength = 1;
			}

			$totalCost = $stringLength * env("averageSMSCost");

			$batch_size = 1;

			// $unitBalance = SMS::getBalance($senderId);
			//
			// $balanceLimit = Brandnames::checkMinimumLimit($senderId);
			//
			// if($unitBalance > $balanceLimit && $totalCost < $unitBalance)
			// {
			// 	$overall_status = 1;
			// }
			// else
			// {
			// 	$overall_status = 0;
			// }

			$add = SMS::queueSMS($message,0,$send_date,$send_time,$batch_size,$totalCost,$mobileNo);
			if($add)
			{

				// if($unitBalance > $balanceLimit && $totalCost < $unitBalance)
				// {
				// 	SMS::sendSMS($mobileNo,$message);
				// 	return Redirect::back()->with(['status1'=>'Your message was sent successfully.']);
				// }

				return Redirect::back()->with(['status1'=>'Your message was queued successfully.']);

			}
			else
			{
					return Redirect::back()->with(['status0'=>'Error occurred while queuing. Please try again']);
			}

	}

	public function smsgroup(Request $request)
	{
			//$senderId = $request->senderId;
			$message = $request->message;
			$adminId	= Auth::user()->adminId;
			$groupId = $request->groupId;

			$send_date = $request->send_date;
			$send_time = $request->send_time;

			$batch_size = Members::checkSize($groupId);

			$stringLength = strlen($message);
			if($stringLength > 160)
			{
				$stringLength = ceil($stringLength / 160);
			}
			else
			{
				$stringLength = 1;
			}

			$totalCost = ($stringLength * env("averageSMSCost")) * $batch_size;

			// $unitBalance = SMS::getBalance($senderId);
			//
			// $balanceLimit = Brandnames::checkMinimumLimit($senderId);
			//
			// if($unitBalance > $balanceLimit && $totalCost < $unitBalance)
			// {
			// 	$overall_status = 1;
			// }
			// else
			// {
			// 	$overall_status = 0;
			// }

			$add = SMS::queueSMS($message,$groupId,$send_date,$send_time,$batch_size,$totalCost);
			if ($add)
			{

				// if($overall_status == 0)
				// {
				// 	return Redirect::back()->with(['status1'=>'Your messages have been queued successfully. They will be sent out after you topup your account balance']);
				// }
				// else
				// {
					return Redirect::back()->with(['status1'=>'Your messages have been queued successfully.']);
				//}

			}
			else
			{
					return Redirect::back()->with(['status0'=>'Error occurred while queuing. Please try again']);
			}

	}

//https://www.positronx.io/laravel-import-expert-excel-and-csv-file-tutorial-with-example/
	public function smsexcel(Request $request)
	{

				$groupId = Groups::createExcelGroup();

				$res =	Excel::import(new MembersImport, $request->file('file'));
				if($res) {

					/////////////////////////////////////////////////////////////////////////////////////////////////////
					$senderId = $request->senderId;
					$message = $request->message;
					$adminId	= Auth::user()->adminId;

					$send_date = $request->send_date;
					$send_time = $request->send_time;

					$batch_size = Members::checkSize($groupId);

					$stringLength = strlen($message);
					if($stringLength > 160)
					{
						$stringLength = ceil($stringLength / 160);
					}
					else
					{
						$stringLength = 1;
					}

					$totalCost = ($stringLength * env("averageSMSCost")) * $batch_size;

					$unitBalance = SMS::getBalance($senderId);

					$balanceLimit = Brandnames::checkMinimumLimit($senderId);

					if($unitBalance > $balanceLimit && $totalCost < $unitBalance)
					{
						$overall_status = 1;
					}
					else
					{
						$overall_status = 0;
					}

					$add = SMS::queueSMS($message,$senderId,$groupId,$send_date,$send_time,$overall_status,$batch_size,$totalCost);
					if ($add)
					{

						if($overall_status == 0)
						{
							return Redirect::back()->with(['status1'=>'Your messages have been queued successfully. They will be sent out after you topup your account balance']);
						}
						else
						{
							return Redirect::back()->with(['status1'=>'Your messages have been queued successfully.']);
						}

					}
					else
					{
							return Redirect::back()->with(['status0'=>'Error occurred while queuing. Please try again']);
					}
					////////////////////////////////////////////////////////////////////////////////////////////////////

				}
				else {
					return Redirect::back()->with(['status0'=>'Error occurred while queuing. Please try again']);
				}

			}

}
