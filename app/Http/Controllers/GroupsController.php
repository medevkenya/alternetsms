<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Groups;
use App\Members;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\GroupsExport;

class GroupsController extends Controller {

	public function Groups()
{
	return view('groups');
}

public function viewgroup($id)
{
	$details = Groups::getDetails($id);
	if($details)
	{
		$contacts = Members::getAllByGroup($id);
		$groups = Groups::getAll(0);
		return view('viewgroup',['details'=>$details,'contacts'=>$contacts,'groups'=>$groups]);
	}
	else
	{
		return Redirect::to('groups')->with(['status0'=>'Group not found.']);
	}
}

public function exportgroups($type)
{
	return Excel::download(new GroupsExport, 'groups.xlsx');
}

public function addgroup(Request $request)
{

  $groupName = $request->groupName;
  $description = $request->description;
  $adminId	= Auth::user()->adminId;
  $created_by	= Auth::user()->id;

	$check = DB::table('groups')->where('groupName',$groupName)->where('adminId',$adminId)->first();
	if($check)
  {
		return Redirect::to('groups')->with(['status0'=>'Group name already exists.','groupName'=>$groupName]);
	}
	else
  {

    	$add = DB::insert('insert into groups (adminId,created_by,groupName,description) values (?,?,?,?)', [$adminId,$created_by,$groupName,$description]);
    	if($add)
      {
    		return Redirect::to('groups')->with(['status1'=>'New group was created successfully.']);
    	}
    	else
      {
    		return Redirect::to('groups')->with(['status0'=>'Error occurred while creating record.']);
    	}

  }

}


public function editgroup(Request $request)
{
	$id = $request->id;
	$groupName = $request->groupName;
  $description = $request->description;
  $adminId	= Auth::user()->adminId;

	$check = DB::table('groups')->where('groupName',$groupName)->where('adminId',$adminId)->where('id','!=',$id)->first();
	if($check) {
		return Redirect::to('groups')->with(['status0'=>'Group name already exists.']);
	}
	else {

  	$toggle = DB::table('groups')->where('id', $id)->update(['groupName' => $groupName,'description' => $description]);
  	if($toggle) {
  		return Redirect::to('groups')->with(['status1'=>'The record was updated successfully']);
  	}
  	else {
  		return Redirect::to('groups')->with(['status0'=>'Error occurred while updating record']);
  	}

  }

}

public function deletegroup(Request $request)
{
  $id = $request->id;
	$delete = DB::table('groups')->where('id', $id)->update(['isDeleted' => 1]);
	if($delete) {
		return Redirect::to('groups')->with(['status1'=>'Record was deleted successfully']);
	}
	else {
		return Redirect::to('groups')->with(['status0'=>'Error occurred while deleting record']);
	}
}


}
