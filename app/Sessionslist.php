<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Sessionslist extends Model
{
    protected $table = 'sessions_list';

    public static function add($all)
   {
    $model = new Sessionslist;
    $model->name = $all['name'];
    $model->description = $all['description'];
    $model->created_by = Auth::user()->id;
    $model->save();
    if($model) {
      return true;
    }
    else {
      return false;
    }
  }

   public static function edit($all)
   {
      $model = Sessionslist::find($all['id']);
      $model->name = $all['name'];
      $model->description = $all['description'];
      $model->save();
      if($model) {
        return true;
      }
      else {
        return false;
      }
    }

    public static function deletesession($all)
    {
      $model = Sessionslist::find($all['id']);
      $model->isDeleted = 1;
      $model->save();
      if($model) {
        return true;
      }
      else {
        return false;
      }
    }

    public static function getAll() {
        return Sessionslist::where('isDeleted',0)->orderBy('id','DESC')->paginate(10);
    }

}
