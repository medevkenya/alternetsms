<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Groups extends Model
{
    protected $table = 'groups';

    public static function countAll()
    {
        return Groups::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
    }

    public static function countAllGeneral()
    {
        return Groups::where('isDeleted',0)->count();
    }

    public static function getDetails($id)
    {
      $res = Groups::where('id',$id)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->first();
      if($res) {
        return $res;
      }
      else {
        return false;
      }
    }

    public static function getAll($limit)
    {
        if($limit == 0) {
          return Groups::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->paginate(10);
        }
        else {
        return Groups::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->limit($limit)->get();
      }
    }

    public static function createExcelGroup()
    {
      $ii= strtoupper(str_shuffle(time().Auth::user()->adminId."abcdefghijklmnopqrstuvwxyz"));
      $password = substr($ii, -6);

      $groupName = "Excel Group ".$password." - ".date('Y-m-d');

      $model = new Groups;
      $model->groupName = $groupName;
      $model->adminId = Auth::user()->adminId;
      $model->created_by = Auth::user()->id;
      $model->save();
      Activities::saveLog("Create excel group [".$groupName."]");
      return $model->id;
    }

}
