<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class Outbox extends Model
{
    protected $table = 'outbox';

    public static function getDashboard()
    {
        return Outbox::select('outbox.mobileNo','outbox.message','outbox.cost','outbox.created_at','groups.groupName','contacts.contactName')
        ->leftJoin('groups','outbox.groupId','=','groups.id')
        ->leftJoin('contacts','outbox.contactId','=','contacts.id')
        ->where('outbox.adminId',Auth::user()->adminId)->where('outbox.isDeleted',0)->limit(6)->get();
    }

    public static function getAll()
    {
        return Outbox::select('outbox.mobileNo','outbox.message','outbox.cost','outbox.created_at','groups.groupName','contacts.contactName','brandnames.brandName')
        ->leftJoin('groups','outbox.groupId','=','groups.id')
        ->leftJoin('contacts','outbox.contactId','=','contacts.id')
        ->leftJoin('brandnames','outbox.senderId','=','brandnames.id')
        ->where('outbox.adminId',Auth::user()->adminId)->where('outbox.isDeleted',0)->paginate(50);
    }

    public static function getAllByContactId($contactId)
    {
        return Outbox::select('outbox.mobileNo','outbox.message','outbox.cost','outbox.created_at','groups.groupName','contacts.contactName','brandnames.brandName')
        ->leftJoin('groups','outbox.groupId','=','groups.id')
        ->leftJoin('contacts','outbox.contactId','=','contacts.id')
        ->leftJoin('brandnames','outbox.senderId','=','brandnames.id')
        ->where('outbox.contactId',$contactId)
        ->where('outbox.adminId',Auth::user()->adminId)
        ->where('outbox.isDeleted',0)->paginate(50);
    }

    public static function countAll()
    {
      return Outbox::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
    }

    public static function countAllGeneral()
    {
      return Outbox::where('isDeleted',0)->count();
    }

    public static function countAllByGroup($groupId)
    {
      return Outbox::where('groupId',$groupId)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
    }

    public static function count()
    {
      return Outbox::where('isDeleted',0)->count();
    }

    public static function getAllByMonth() {
      $items = Outbox::select(
                    DB::raw("(COUNT(*)) as count"),
                    DB::raw("MONTHNAME(created_at) as month_name")
                )->whereYear('created_at', date('Y'))
                ->groupBy('month_name')
                ->get()
                ->toArray();
                if(empty($items)) {
                  $items = array(
                    array('month_name'=>'January','count'=>"0 - Not Set"),
                    array('month_name'=>'February','count'=>"0 - Not Set"),
                    array('month_name'=>'March','count'=>"0 - Not Set"),
                    array('month_name'=>'April','count'=>"0 - Not Set"),
                    array('month_name'=>'May','count'=>"0 - Not Set"),
                    array('month_name'=>'June','count'=>"0 - Not Set"),
                    array('month_name'=>'July','count'=>"0 - Not Set"),
                    array('month_name'=>'August','count'=>"0 - Not Set"),
                    array('month_name'=>'September','count'=>"0 - Not Set"),
                    array('month_name'=>'October','count'=>"0 - Not Set"),
                    array('month_name'=>'November','count'=>"0 - Not Set"),
                    array('month_name'=>'December','count'=>"0 - Not Set")
                  );
                }
                return $items;
    }

}
