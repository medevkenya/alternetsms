<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Payments;

class Subscriptions extends Model
{
    protected $table = 'subscriptions';

    public static function trialSubscription($adminId) {
      $subscriptionDate = date('Y-m-d');
      $expiryDate = date('Y-m-d', strtotime($subscriptionDate . " + 1 day"));
      $model = new Subscriptions;
      $model->subscriptionDate = $subscriptionDate;
      $model->expiryDate = $expiryDate;
      $model->adminId = $adminId;
      $model->created_by = $adminId;
      $model->save();
    }

    public static function updateDates($reference,$transaction_id) {

      $paydetails = Payments::select('payments.*','packages.months')
      ->leftJoin('packages','payments.packageId','=','packages.id')
      ->where('payments.reference',$reference)->where('payments.transaction_id',$transaction_id)
      ->where('payments.status',1)->first();
      if($paydetails) {
        $adminId	= Auth::user()->id;
        $packageId = $paydetails->packageId;

        $subscriptionDate = date('Y-m-d');
        $expiryDate = date('Y-m-d', strtotime("+".$paydetails->months." months", strtotime($subscriptionDate))); //date('Y-m-d', strtotime($subscriptionDate. ' + 30 days'));

        $resSub = Subscriptions::where('adminId',$adminId)->first();
        if($resSub) {

          if($resSub->status == 1)
          {
            $expiryDate = date('Y-m-d', strtotime("+".$paydetails->months." months", strtotime($resSub->expiryDate)));
          }

          self::saveSub($packageId,$subscriptionDate,$expiryDate);
        }
        else {
          self::saveNew($packageId,$subscriptionDate,$expiryDate);
        }

        return true;
      }
      else {
        return false;
      }

    }

    public static function saveNew($packageId,$subscriptionDate,$expiryDate) {
      $adminId	= Auth::user()->id;
      $model = new Subscriptions;
      $model->adminId = $adminId;
      $model->packageId = $packageId;
      $model->subscriptionDate = $subscriptionDate;
      $model->expiryDate = $expiryDate;
      $model->save();
      if($model) {
        return true;
      }
      else {
        return false;
      }
    }

    public static function saveSub($packageId,$subscriptionDate,$expiryDate) {
      $adminId	= Auth::user()->id;
      $resSub = Subscriptions::where('adminId',$adminId)->first();
      $model = Subscriptions::find($resSub->id);
      $model->packageId = $packageId;
      $model->subscriptionDate = $subscriptionDate;
      $model->expiryDate = $expiryDate;
      $model->save();
      if($model) {
        return true;
      }
      else {
        return false;
      }
    }

}
