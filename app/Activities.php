<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Activities extends Model
{
    protected $table = 'activities';

    public static function getAll($limit)
    {
        return Activities::select('activities.*','users.firstName','users.lastName')
        ->leftJoin('users','activities.created_by','=','users.id')
        ->where('activities.adminId',Auth::user()->adminId)->where('activities.isDeleted',0)->orderBy('activities.id','DESC')->limit($limit)->get();
    }

    public static function getAllActivities()
    {
        return Activities::select('activities.*','users.firstName','users.lastName')
        ->leftJoin('users','activities.created_by','=','users.id')
        ->where('activities.adminId',Auth::user()->adminId)->where('activities.isDeleted',0)->orderBy('activities.id','DESC')->paginate(50);
    }

    public static function saveLog($description)
    {
      $adminId	= Auth::user()->adminId;
      $created_by	= Auth::user()->id;
      $model = new Activities;
      $model->description = $description;
      $model->adminId = $adminId;
      $model->created_by = $created_by;
      $model->save();
    }

}
