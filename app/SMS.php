<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use AfricasTalking\SDK\AfricasTalking;
use App\Classes\AfricasTalkingGateway;
use App\Members;
use App\Brandnames;
use App\Activities;

class SMS extends Model
{
    protected $table = 'ref_queue';

    public static function updateone($id, $senderId, $groupId, $message, $send_date,$send_time,$overall_status)
    {

        $stringLength = strlen($message);
  			if($stringLength > 160)
  			{
  				$stringLength = ceil($stringLength / 160);
  			}
  			else
  			{
  				$stringLength = 1;
  			}

        $batch_size = Members::checkSize($groupId);

  			$totalCost = ($stringLength * env("averageSMSCost")) * $batch_size;

        $model = SMS::find($id);
        $model->message = $message;
        $model->senderId = $senderId;
        $model->groupId = $groupId;
        $model->send_date = $send_date;
        $model->send_time = $send_time;
        $model->overall_status = $overall_status;
        $model->totalCost = $totalCost;
        $model->save();
        if ($model)
        {
            Activities::saveLog("Edited SMS batch [".$id."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = SMS::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model)
        {
            Activities::saveLog("Deleted SMS batch [".$id."]");
            return true;
        }
        return false;
    }

    public static function getAll()
    {
        return SMS::select('ref_queue.*','groups.groupName','brandnames.brandName')
        ->leftJoin('groups','ref_queue.groupId','=','groups.id')
        ->leftJoin('brandnames','ref_queue.senderId','=','brandnames.id')
        ->where('ref_queue.adminId',Auth::user()->adminId)
        ->where('ref_queue.isDeleted',0)
        ->orderBy('ref_queue.id','DESC')
        ->paginate(50);
    }

    public static function queueSMS($message,$groupId,$send_date,$send_time,$batch_size,$totalCost,$mobileNo=0)
    {

      $checkUser = Brandnames::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->first();
      $senderId = $checkUser->brandName;

      $model = new SMS;
      $model->message = $message;
      $model->senderId = $senderId;
      $model->groupId = $groupId;
      $model->send_date = $send_date;
      $model->send_time = $send_time;
      //$model->overall_status = $overall_status;
      $model->batch_size = $batch_size;
      $model->adminId = Auth::user()->adminId;
      $model->created_by = Auth::user()->id;
      $model->totalCost = $totalCost;
      $model->save();
      if($model)
      {
        Members::logMessages($model->id,$message,$senderId,$groupId,$mobileNo);
        return true;
      }
      else
      {
        return false;
      }
    }

    public static function getBalance()
    {
        // return $totalBalance = 100;
        $keyb = Brandnames::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->first();
        // foreach ($list as $keyb) {

        // Set your app credentials
        $username   = $keyb->username;
        $apikey     = $keyb->apikey;

        // Create a new instance of our awesome gateway class
        $gateway    = new AfricasTalkingGateway($username, $apikey);
        // Any gateway errors will be captured by our custom Exception class below,
        // so wrap the call in a try-catch block
        try
        {
        // Fetch the data from our USER resource and read the balance
        $data = $gateway->getUserData();
        //echo "Balance: " . $data->balance."\n";
        // The result will have the format=> KES XXX
        //$totalBalance += $currentbalance = $data->balance;
        return $data->balance;

        }
        catch ( AfricasTalkingGatewayException $e )
        {
        return null;
        }

      // }
      // return $totalBalance;
    }

    /**
  * Send sms now
  */
  public static function sendSMS($mobileNo,$message) {

  // Specify your login credentials
  $username   = "FLECASAPP";
  $apiKey     = "52dbb92a33d91fc82c2befeab42f227923435ec6952bf4d0ca3dbb";
  $senderId = "FLECBSKE";

  $AT       = new AfricasTalking($username, $apiKey);

  // Get one of the services
  $sms      = $AT->sms();

  // Use the service
  $result   = $sms->send([
    'from'      => $senderId,
    'to'        => $mobileNo,
    'message'   => $message
  ]);

  return true;
  //print_r($result);

  }

//   public static function CronSendCounty()
// {
//   date_default_timezone_set("Africa/Nairobi");
//   ini_set('max_execution_time', 999999999);
//     //Specify your login credentials
// 	$username   = "FLSAPP";
//   $apiKey     = "52dbb92befeab42f227923435ec6952bf4d0ca3dbb";
//   $senderId = "FLEBSKE";
//
//   log::info("CronSendCounty--".date('Y-m-d H:i:s')."--");
//
// 	$date= date('Y-m-d');
// 	$time = date('H:i');
// 	$checkbatch = DB::table('ref_queue')->where('overall_status','0')->where('send_date','=',$date)->where('send_time','<=',$time)->orderBy('id','DESC')->first();
// 	if(!$checkbatch) { exit(); }
// 	$ref_queue_id = $checkbatch->id;
// 	$senderId = $checkbatch->senderId;
// 	$message = $checkbatch->message;
//
// 	$batch_limit_size = 1000;
//     $maincustomers = DB::table('messages')->where('ref_queue_id', $ref_queue_id)->where('status','0')->limit($batch_limit_size)->get();
//
// 	$lines = array();
// 	$count_processed = 0;
// 	foreach( $maincustomers as $customer ) {
// 	$phone = $customer->phone;
// 	$newphone =  substr($phone, -9);
// 	$phone = "254".$newphone;
// 	$lines[] = $phone;
// 	$count_processed++;
//
// 	//DB::insert('insert into messages_sent (phone,ref_queue_id,status,message) values (?,?,?,?)', [$newphone,$ref_queue_id,1,$message]);
// 	DB::table('messages')->where('ref_queue_id', $ref_queue_id)->where('phone',$newphone)->delete();
//
// 	}
//
// 	if(count($lines)>1){
// 		$recipients = implode(',', $lines);
// 	}else{
// 		$recipients =$lines[0];
// 	}
//
// 	$gateway    = new AfricasTalkingGateway($username, $apikey);
//
// 	try
// 	{
//
// 	$results = $gateway->sendMessage($recipients, $message,$senderId);
//
// 	foreach($results as $result) {
//
// 	$msglen=strlen($message);
// 	$multipl = ceil($msglen/160);
// 	$costt = $multipl * 1;
//
// 	$phone = $result->number;
// 	$phone =  substr($phone, -9);
// $status = $result->status;
// $messageid = $result->messageId;
// //echo " Cost: "   .$result->cost."\n";
//
// $getbatch = DB::table('ref_queue')->where('id', $ref_queue_id)->first();
//
// //if($status == "Success" || $status == "Sent") {
// 	//DB::table('messages')->where('phone', $phone)->where('ref_queue_id', $ref_queue_id)->update(['status' => 1]);
// 	$addsent = DB::insert('insert into messages_sent (mobileNo,ref_queue_id) values (?,?)', [$phone,$ref_queue_id]);
// 	DB::table('messages')->where('ref_queue_id', $ref_queue_id)->where('phone',$phone)->delete();
// 	$batch_sent = $getbatch->batch_sent + 1;
// 	DB::table('ref_queue')->where('id', $ref_queue_id)->update(['batch_sent' => $batch_sent]);
// //}
// //else {
// 	//DB::table('messages')->where('phone', $phone)->where('ref_queue_id', $ref_queue_id)->update(['status' => 3]);
// 	//DB::table('ref_queue')->where('phone', $phone)->where('ref_queue_id', $ref_queue_id)->update(['status' => 1]);
// //}
//
// 	}
//
// 	$getbatch = DB::table('ref_queue')->where('id', $ref_queue_id)->first();
// 	$batch_processed = $getbatch->batch_processed + $count_processed;
// 	$batch_size= $getbatch->batch_size;
// 	DB::table('ref_queue')->where('id', $ref_queue_id)->update(['batch_processed' => $batch_processed]);
//
// 	if($batch_processed >= $batch_size) {
// 		DB::table('ref_queue')->where('id', $ref_queue_id)->update(['overall_status' => 1]);
// 	}
//
// 	}
// 	catch ( AfricasTalkingGatewayException $e ) {
// 		echo "Encountered an error while sending: ".$e->getMessage();
// 	}
// 	// DONE!!!
//
// }

}
